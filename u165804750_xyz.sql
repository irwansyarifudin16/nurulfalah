-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 16 Jan 2017 pada 10.07
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `donatur`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(10) NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `username_admin` varchar(50) NOT NULL,
  `password_admin` varchar(50) NOT NULL,
  `email_admin` varchar(100) NOT NULL,
  `no_tlp_admin` int(11) DEFAULT NULL,
  `alamat_admin` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `nama_admin`, `username_admin`, `password_admin`, `email_admin`, `no_tlp_admin`, `alamat_admin`) VALUES
(1, 'Laila', 'laila2016', 'laila123', 'laila@gmail.com', NULL, NULL),
(3, 'halo', 'halobandung', 'halo', 'halo@yahoo.com', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kegiatan`
--

CREATE TABLE `kegiatan` (
  `id_kegiatan` int(10) NOT NULL,
  `nama_kegiatan` varchar(100) NOT NULL,
  `tgl_kegiatan` date NOT NULL,
  `keterangan` text NOT NULL,
  `foto_kegiatan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kegiatan`
--

INSERT INTO `kegiatan` (`id_kegiatan`, `nama_kegiatan`, `tgl_kegiatan`, `keterangan`, `foto_kegiatan`) VALUES
(2, 'SD Pesona Indah', '2010-01-01', '                Kegiatan belajar di dalam kelas beralaskan tikar tanpa meja dan kursi.', 'b.jpg'),
(3, 'SD Merpati Putih', '2011-03-03', 'Kegiatan belajar mengajar dengan atap terbuka dan ruang kelas yang lusuh.', 'e.jpg'),
(4, 'SD Ceria', '2010-06-03', 'Dengan baju seala kadarnya, Siswa SD Ceria tetap menyimpan semangat yang penuh demi masa depannya.', 'n.jpg'),
(6, 'SDN Nurul Falah', '1980-07-07', ' Merayakan acara 17 Agustus dengan lomba sederhana dan bermakna.', 'm.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `non_user`
--

CREATE TABLE `non_user` (
  `id_transaksi_non_user` int(5) NOT NULL,
  `nama_non_user` varchar(50) NOT NULL,
  `email_non_user` varchar(50) NOT NULL,
  `no_tlp_non_user` int(20) NOT NULL,
  `tgl_transaksi_non_user` date NOT NULL,
  `jml_transaksi_non_user` int(100) NOT NULL,
  `bank_non_user` varchar(100) NOT NULL,
  `no_rek_non_user` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `non_user`
--

INSERT INTO `non_user` (`id_transaksi_non_user`, `nama_non_user`, `email_non_user`, `no_tlp_non_user`, `tgl_transaksi_non_user`, `jml_transaksi_non_user`, `bank_non_user`, `no_rek_non_user`) VALUES
(1, 'Marwan', 'marwan@gmail.com', 878374288, '2016-12-16', 16000, '0', 12345678),
(2, 'Oki', 'Oki@gmail.com', 878374288, '2016-12-16', 16000, '0', 12345678),
(3, 'amaliyah', 'amaliyah@gmail.com', 875534059, '2016-12-16', 17000, 'PERMATA BANK', 2147483647);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesan`
--

CREATE TABLE `pesan` (
  `id_pesan` int(5) NOT NULL,
  `nama_pengirim` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subjek` varchar(100) NOT NULL,
  `pesan` text NOT NULL,
  `tgl_pesan` date NOT NULL,
  `status` enum('baru','lama') NOT NULL,
  `id_user` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pesan`
--

INSERT INTO `pesan` (`id_pesan`, `nama_pengirim`, `email`, `subjek`, `pesan`, `tgl_pesan`, `status`, `id_user`) VALUES
(8, 'Melia', 'melia2@gmail.com', 'Kepercayaan', 'Percaya', '2016-12-26', 'baru', 1),
(10, 'Irwan Syarifudin', 'irwansyarifudin@yahoo.com', 'isi subjek pesan', 'Adaa apa ?', '2016-12-31', 'baru', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekolah`
--

CREATE TABLE `sekolah` (
  `id_sekolah` int(10) UNSIGNED NOT NULL,
  `nama_sekolah` varchar(25) NOT NULL,
  `alamat_sekolah` varchar(50) NOT NULL,
  `no_tlp_sekolah` int(25) NOT NULL,
  `pjs` varchar(20) NOT NULL,
  `keterangan_sekolah` text NOT NULL,
  `foto_sekolah` varchar(255) NOT NULL,
  `status_usulan` varchar(20) NOT NULL,
  `id_user` int(5) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sekolah`
--

INSERT INTO `sekolah` (`id_sekolah`, `nama_sekolah`, `alamat_sekolah`, `no_tlp_sekolah`, `pjs`, `keterangan_sekolah`, `foto_sekolah`, `status_usulan`, `id_user`) VALUES
(9, 'Irwan Syarifudin', 'Jalan Agung Raya ', 217836472, 'Mama dan AA', 'Asri', 'IMG-20161215-WA0003.jpg', 'Diterima', 1),
(10, 'SDN 09 Sore', 'depok', 2147483647, 'Irwan', 'adjkaljd', 'lari4.jpg', 'Diterima', 1),
(11, 'sd sd', 'as', 2189, 'nk', 'ls', 'lari1.jpg', 'Diterima', 2),
(12, 'Sekolah Icum', 'Icum Raya', 2147483647, 'Icum Cumi', 'Cumi Goreng', 'lari4.jpg', 'Diterima', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_konfirm_status`
--

CREATE TABLE `tabel_konfirm_status` (
  `nama_admin` varchar(100) DEFAULT NULL,
  `id_transaksi` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Trigger `tabel_konfirm_status`
--
DELIMITER $$
CREATE TRIGGER `status` AFTER INSERT ON `tabel_konfirm_status` FOR EACH ROW BEGIN
UPDATE tabel_konfirm_status set id_transaksi= id_transaksi where id_transaksi AND status_transaksi = 'berhasil' ; 
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(10) UNSIGNED NOT NULL,
  `tgl_transaksi` date NOT NULL DEFAULT '0000-00-00',
  `jml_transaksi` int(12) DEFAULT NULL,
  `nama_bank` varchar(100) DEFAULT NULL,
  `no_rek` int(100) DEFAULT NULL,
  `bukti_transaksi` varchar(255) NOT NULL,
  `status_transaksi` varchar(20) DEFAULT NULL,
  `id_user` int(5) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `tgl_transaksi`, `jml_transaksi`, `nama_bank`, `no_rek`, `bukti_transaksi`, `status_transaksi`, `id_user`) VALUES
(29, '2016-12-27', 200000, 'BRI SYARIAH', 2147483647, '', 'berhasil', 3),
(30, '2016-12-28', 15000, 'BRI SYARIAH', 1234567890, '', '', 0),
(31, '2017-01-04', 90000, 'BRI SYARIAH', 100000, '', 'berhasil', 2),
(32, '2017-01-05', 20000, 'BRI SYARIAH', 123452636, '', 'berhasil', 2),
(33, '2017-01-05', 230000, 'BRI SYARIAH', 121313, 'ibu_laila.jpg', 'berhasil', 2),
(10240, '2017-01-16', 20000, 'BRI SYARIAH', 2323123, '', NULL, 2),
(10403, '2017-01-16', 90000, 'BRI SYARIAH', 88677, '', NULL, 2),
(135010, '2017-01-05', 800000, 'BRI SYARIAH', 126717177, 'Capture.PNG', NULL, 2);

--
-- Trigger `transaksi`
--
DELIMITER $$
CREATE TRIGGER `id_transaksi_new` BEFORE INSERT ON `transaksi` FOR EACH ROW BEGIN
DECLARE newid VARCHAR(8);
SELECT CURTIME()+0 INTO newid;
SET NEW.id_transaksi = newid;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(5) UNSIGNED NOT NULL,
  `nama` varchar(25) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(25) NOT NULL,
  `alamat` text NOT NULL,
  `no_tlp` int(12) NOT NULL,
  `pekerjaan` varchar(20) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama`, `username`, `password`, `email`, `alamat`, `no_tlp`, `pekerjaan`, `foto`) VALUES
(1, 'Melia', 'melia2016', 'melia', 'melia2@gmail.com', 'Bekasi Timur', 898716768, 'Mahasiswi', '++_13.jpg'),
(2, 'Irwan Syarifudin', 'irwan2016', 'irwan2016', '', '', 217883427, '', 'irwan.jpg'),
(3, 'aliudin', 'aliudin2016', 'ali', 'ali@gmail.com', '', 0, '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`);

--
-- Indexes for table `non_user`
--
ALTER TABLE `non_user`
  ADD PRIMARY KEY (`id_transaksi_non_user`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id_pesan`);

--
-- Indexes for table `sekolah`
--
ALTER TABLE `sekolah`
  ADD PRIMARY KEY (`id_sekolah`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `id_kegiatan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `non_user`
--
ALTER TABLE `non_user`
  MODIFY `id_transaksi_non_user` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id_pesan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `sekolah`
--
ALTER TABLE `sekolah`
  MODIFY `id_sekolah` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135011;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
