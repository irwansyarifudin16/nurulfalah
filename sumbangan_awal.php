<?php
include "koneksi.php";  
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Tambah Sumbangan</title>  
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/creative.css" rel="stylesheet">
    <link rel="stylesheet"  href="css/animate.css">
    <link rel="stylesheet"  href="css/style.css">

</head>

<body bgcolor="#F05F40">
<div class="container-fluid" style="background-color: black">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="donasi_awal.php">Kembali</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="index.php#page-top">Beranda</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#program">Program</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#dokumentasi">Dokumentasi</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#tentangkami">Tentang Kami</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#hubungikami">Hubungi Kami</a>
                    </li>
                    <li>
                        <a style="background-color:transparent;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Masuk</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div> 

<div class="container">
        <div class="row">
            <div class="text-center" >
           <!--   <img src="img/logo.jpeg"> -->
              <br>
              <h2 class="section-heading">Sumbang sekarang !</h2>
                <hr class="primary">
            </div>
        </div>
  <form method="post" action="proses_sumbang_barang_nonuser.php" enctype="multipart/form-data">
    
      <div class="from-group col-md-offset-4 col-md-4">
            <label class="label-username" name="username">
                    Isi nama kamu    
                    <br>
                </label>
                <br>
                <input type="text" name="nama_nonuser" class="form-control" placeholder="nama kamu" required>
            </div>

            <div class="from-group col-md-offset-4 col-md-4"><br>
            <label class="label-username" name="email">
                    Email yang aktif    
                    <br>
                </label>
                <br>
                <input type="email" name="email_barang_nonuser" class="form-control" placeholder="isi email yang aktif ya" required>
            </div>   

            <div class="from-group col-md-offset-4 col-md-4"><br>
            <label class="label-username" name="username">
                    Nomor telepon yang bisa dihubungi    
                    <br>
                </label>
                <br>
                <input type="number" name="no_telp_barang_nonuser" class="form-control" placeholder="isi nomor teleponmu" required>
            </div>   
   

            <div class="from-group col-md-offset-4 col-md-4"><br>
                <label class="label-username" name="username">
                    Jenis Barang    
                    <br>
                </label>
                <br>
                <select type="text" name="jenis_barang_nonuser" class="form-control" placeholder="jenis_barang" required>
                <option value="Fasilitas Sekolah">Fasilitas sekolah (bangku,kursi,lemari,dll)</option>
                <option value="Buku">Buku</option>
                <option value="Seragam">Seragam</option>
                <option value="Perlengkapan Sekolah">Perlengkapan Sekolah</option>
                <option value="lain-lain">Lainnya</option>
                <br>
            </div>
            <br>

            <div class="from-group col-md-offset-4 col-md-4">
                <br>
                <input type="text" name="nama_barang_nonuser" class="form-control" placeholder="isi barang" required><br>
            </div>   

            <div class="from-group col-md-offset-4 col-md-4">
            <label class="label-username" name="username">
                    Banyaknya barang    
                    <br>
                </label>
                <br>
                <input type="number" name="jumlah_barang_nonuser" class="form-control" placeholder="masukan jumlah barang" required>
            </div>

      <div class="from-group col-md-offset-4 col-md-4">
      <label class="label-email" name="email">
      <br>
      Upload  foto/video Barang Sumbangan
      </label>  
      <input type="file" name="file" class="validate" value="masukan foto" required><br>
      </div>
      <div class="from-group col-md-offset-4 col-md-4">
      <input type="submit" name="lanjut" value="Sumbang !" class="btn waves-effect waves-light col s12">
      <br>
      </div>

    </form>
    </div>

<!--<form method="post" action="" >
 <p><input type="radio" onclick="javascript:yesnoCheck();" name="yesno" id="noCheck" checked><strong>Automatic</strong>
 <input type="radio" onclick="javascript:yesnoCheck();" name="yesno" id="yesCheck"><strong>Custom</strong>
 </p>
 <p><labe><strong> Url:</strong></label><input type="text" name="url" size="45" />
 <span id="ifYes" style="display:none" ><strong> Custom:</strong><input type="text" name="custom" size="45" class="icustom"/><br/>
 <p><input type="submit" name="submit" id="Submit" value="Shorten" /></p>
 <div id="result"></div>
 </form> -->

</body>

</html>