<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>

  img {
      border: 1px solid #424242;
      border-radius: 6px;
      padding: 5px;
  }

 .nav-side-menu {
    overflow: auto;
    font-family: verdana;
    font-size: 12px;
    font-weight: 200;
    background-color: #2e353d;
    position: fixed;
    top: 0px;
    width: 300px;
    height: 100%;
    color: #e1ffff;
}
.nav-side-menu .brand {
    background-color: #F05F40;
    line-height: 50px;
    display: block;
    text-align: center;
    font-size: 14px;
}
.nav-side-menu .toggle-btn {
    display: none;
  }
.nav-side-menu ul,
.nav-side-menu li {
    list-style: none;
    padding: 0px;
    margin: 0px;
    line-height: 35px;
    cursor: pointer;
}
.nav-side-menu ul :not(collapsed) .arrow:before,
.nav-side-menu li :not(collapsed) .arrow:before {
    font-family: FontAwesome;
    content: "\f078";
    display: inline-block;
    padding-left: 10px;
    padding-right: 10px;
    vertical-align: middle;
    float: right;
  }
.nav-side-menu ul .active,
.nav-side-menu li .active {
    border-left: 3px solid #d19b3d;
    background-color: #4f5b69;
}
.nav-side-menu ul .sub-menu li.active,
.nav-side-menu li .sub-menu li.active {
    color: #d19b3d;
}
.nav-side-menu ul .sub-menu li.active a,
.nav-side-menu li .sub-menu li.active a {
    color: #d19b3d;
}
.nav-side-menu ul .sub-menu li,
.nav-side-menu li .sub-menu li {
    background-color: #181c20;
    border: none;
    line-height: 28px;
    border-bottom: 1px solid #23282e;
    margin-left: 0px;
}
.nav-side-menu ul .sub-menu li:hover,
.nav-side-menu li .sub-menu li:hover {
    background-color: #020203;
}

div.content {
    margin-left: 25%;
    padding: 1px 16px;
    height: 1000px;
}



.nav-side-menu ul .sub-menu li:before,
.nav-side-menu li .sub-menu li:before {
    font-family: FontAwesome;
    content: "\f105";
    display: inline-block;
    padding-left: 10px;
    padding-right: 10px;
    vertical-align: middle;
}
.nav-side-menu li {
    padding-left: 0px;
    border-left: 3px solid #2e353d;
    border-bottom: 1px solid #23282e;
}
.nav-side-menu a {
    text-decoration: none;
    color: #e1ffff;
}
.nav-side-menu a i {
    padding-left: 10px;
    width: 20px;
    padding-right: 20px;
}

.nav-side-menu li a {
    text-decoration: none;
    color: #e1ffff;
}
.nav-side-menu li a i {
    padding-left: 10px;
    width: 20px;
    padding-right: 20px;
}
.nav-side-menu li:hover {
    border-left: 3px solid #d19b3d;
    background-color: #4f5b69;
    -webkit-transition: all 1s ease;
    -moz-transition: all 1s ease;
    -o-transition: all 1s ease;
    -ms-transition: all 1s ease;
    transition: all 1s ease;
}
@media (max-width: 767px) {
  .nav-side-menu {
    position: relative;
    width: 100%;
    margin-bottom: 10px;
  }
  .nav-side-menu .toggle-btn {
    display: block;
    cursor: pointer;
    position: absolute;
    right: 10px;
    top: 10px;
    z-index: 10 !important;
    padding: 3px;
    background-color: #ffffff;
    color: #000;
    width: 40px;
    text-align: center;
  }
  .brand {
    text-align: left !important;
    font-size: 22px;
    padding-left: 20px;
    line-height: 50px !important;
  }
}
@media (min-width: 767px) {
  .nav-side-menu .menu-list .menu-content {
    display: block;
  }
}
body {
    font-family: verdana;
    font-size: 12px;
    margin: 0px;
    padding: 0px;
  }

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align:center;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #ff7043;
    color: white;
    text-align: center
}

</style>

<style>
.button {
    display: inline-block;
    padding: 7px 18px;
    font-size: 12px;
    cursor: pointer;
    text-align: center;
    text-decoration: none;
    outline: none;
    color: #fff;
    background-color: #ff7043;
    border: none;
    border-radius: 15px;
    box-shadow: 0 4px #999;
  }


.button:hover {background-color: #424242}

.button:active {
    background-color: #424242;
    box-shadow: 0 5px #666;
    transform: translateY(4px);
}
</style>

<style>
#rcorners1 {
      cursor: pointer;
      border-radius: 25px;
      border: 2px solid #424242;
      padding: 20px; 
      width: 800px;
      height: 120px;   
      box-shadow: 0 5px #ff7043;
  }

#rcorners1:hover {background-color: #424242;color: #fff;
}

#rcorners1:active {
    color: #fff;
    background-color: #424242;
    box-shadow: 0 5px #666;

    transform: translateY(4px);
  }


#rcorners2 {
     border-radius: 25px;
      box-shadow: 0 5px #ff7043;
      border: 2px solid #424242;
      padding: 20px; 
      width: 800px;
      height: 150px       
  }

#rcorners2:hover {background-color: #424242;color: #fff;}

#rcorners2:active {
    color: #fff;
    background-color: #424242;
    box-shadow: 0 5px #666;

    transform: translateY(4px);
  }


p {
    text-indent: 50px;
    font-family: arial;
    font-size: 14px;
    text-align: justify;
    letter-spacing: 1px;
}
</style>
	<title></title>
</head>
<body>

</body>
</html>

<?php
	echo "<br>";
	echo "<b>Cara untuk mengusulkan Sekolah<br><hr></b>";
	echo "1. Kamu mengetahui kondisi Sekolah yang akan kamu usulkan<br>";
	echo "2. Sekolah tersebut memang layak untuk diberikan bantuan. Baik dari segi fasilitas, dan biaya sekolah siswa yang kurang mampu<br>";
	echo "3. Kamu memiliki kontak person pihak sekolah yang dapat dihubungi<br>";
	echo "4. Kamu mengetahui alamat lengkap Sekolah<br><br>";

	echo "<a href='usul_sekolah.php'><button class='button'>Usulkan sekolah</button></p></a>";
?>