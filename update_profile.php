<?php 
include "session.php";
?>


<html>
<head>
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/creative.css" rel="stylesheet">
    <link rel="stylesheet"  href="css/animate.css">
    <link rel="stylesheet"  href="css/style.css">
    
<title>Update Profil</title>
</head>
<body bgcolor="#F05F40">
<center>
<form action="proses_update_profile.php" method="post">
<table border="0">
<body>
<tr>
<td colspan="2" align="center" >
<h1>Update Profile</h1><br>
</td>
</tr>
<tr>
<td>Nama lengkap</td>
<td>: <input name="nama" type="text" value="<?php echo $nama; ?>" /></td>
<td></td>
</tr>
<tr>
<td>Username</td>
<td>: <input name="username" type="text" value="<?php echo $username; ?>"/></td>
</tr>
<tr>
<td>Email</td>
<td>: <input name="email" type="text" value="<?php echo $email; ?>"/></td>
</tr>
<tr>
<td>Nomor Telepon</td>
<td>: <input name="no_tlp" type="number" value="<?php echo $no_tlp; ?>"/></td>
</tr>
<tr>
<td>Alamat</td>
<td>: <input name="alamat" type="text" value="<?php echo $alamat; ?>"/></td>
</tr>
<tr>
<td>Pekerjaan</td>
<td>: <input name="pekerjaan" type="text" value="<?php echo $pekerjaan; ?>" /></td>
</tr>
<tr><br>
<td>Konfirmasi password</td>
<td>: <input name="password_dulu" type="password" required/></td>
</tr>
<tr><td>&nbsp</td><td>&nbsp</td></tr>
<tr><td><input type="reset" value="Batal" class="btn btn-primary col-md-12" /></td>
<td><input type="submit" value="update" name="update" class="btn btn-primary col-md-12"/></td>
</tr>
</tbody>
</table>
</form>
<br>
<a href = "home.php">Balik ke Beranda</a>
</center>
</html>

