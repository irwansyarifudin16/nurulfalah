<?php 
include "session.php";  
       
?>

<html>
<head>
	<title>Indobisa - Donasi</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/creative.css" rel="stylesheet">
    <link rel="stylesheet"  href="css/animate.css">
    <link rel="stylesheet"  href="css/style.css">
</head>
<body bgcolor="#F05F40">
 	<div class="container">
        <div class="row">
            <div class="text-center">
           <!--  	<img src="img/logo.jpeg"> -->
           		<br>
            	<h2 class="section-heading">Sumbang Barang Yuk !</h2>
               	<hr class="primary">
            </div>
        </div>
      	<form action="proses_sumbang_barang.php" method="POST">
 		
	 	
	 		<div class="from-group col-md-offset-4 col-md-4"><br>
	 			<label class="label-username" name="username">
	 				Jenis Barang	
	 				<br>
	 			</label>
	 			<br>
	 			<select type="text" name="jenis_barang" class="form-control" placeholder="jenis_barang" required>
				<option value="Fasilitas Sekolah">Fasilitas sekolah (bangku,kursi,lemari,dll)</option>
				<option value="Buku">Buku</option>
				<option value="Seragam">Seragam</option>
				<option value="Perlengkapan Sekolah">Perlengkapan Sekolah</option>
				<option value="lain-lain">Lainnya</option>
	 			<br>
	 		</div>
	 		<br>

	 		<div class="from-group col-md-offset-4 col-md-4">
	 			<br>
	 			<input type="text" name="nama_barang" class="form-control" placeholder="isi barang" required><br>
	 		</div>	 

	 		<div class="from-group col-md-offset-4 col-md-4">
	 		<label class="label-username" name="username">
	 				Banyaknya barang	
	 				<br>
	 			</label>
	 			<br>
	 			<input type="number" name="jumlah_barang" class="form-control" placeholder="masukan jumlah barang" required>
	 		</div>	 


	 		<div class="from-group col-md-offset-4 col-md-4">
	 		<br><br>
	 			<center><table border="0"></center>
	 			<tr><td><input type="submit" value="Lanjut" name="lanjut" class="btn btn-primary col-md-12"></td>
	 			<td>&nbsp</td>
	 				<td><input type="reset" value="Batal" name="Reset" class="btn btn-primary col-md-12"></td>
	 				</tr>
	 			</table>
	 		</div>
		</form>
    </div>

 