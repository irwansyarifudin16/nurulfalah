<?php 
include "session.php";
?>

<html>
<head>
	<title>Indobisa - Upload Bukti Transaksi</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/creative.css" rel="stylesheet">
    <link rel="stylesheet"  href="css/animate.css">
    <link rel="stylesheet"  href="css/style.css">

 <style>
.button2 {
  display: inline-block;
  padding: 5px 18px;
  font-size: 12px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: black;
  background-color: white;
  border: 3;
  border-radius: 15px;
  box-shadow: 0 4px #ff7040;
 }
 </style>

</head>
<body bgcolor="#F05F40">
<div class="container-fluid" style="background-color: black">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php#page-top">IndoBisa</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="home.php">kembali ke beranda</a>
                    </li>
                    <li>
                        <a style="background-color:transparent;" type="button" class="btn btn-info btn-lg" href = "logout.php">Keluar</a>
                    </li>
                </ul>
            </div>
            </div>

<br>
 	<div class="container">
        <div class="row">
            <div class="text-center" >
           <!--  	<img src="img/logo.jpeg"> -->
           <!-- <a href="home.php"><button class="button2"><b>Kembali ke Donasi</b></button></a><br><br> -->
           		<br>
              <br>
            	<h2 class="section-heading">Upload Bukti Transaksi</h2>
               	<hr class="primary">
            </div>
        </div>
      	<form action="proses_upload_bukti.php" method="POST" enctype="multipart/form-data">
 		
	 		<div class="from-group col-md-offset-4 col-md-4">
	 			<label class="label-username" name="username">
	 				ID Transaksi
	 				<br>
	 			</label>
	 			<br>
	 			<input type="text" name="id_transaksi" class="form-control" placeholder="Masukan ID transaksi yang terdaftar" required><br>
	 		</div>

	 		<div class="from-group col-md-offset-4 col-md-4">
	 			<label class="label-username" name="username">
	 				File bukti transaksi ( format : jpg & png )
	 				<br>
	 			</label>
	 		<input type="file" name="file" class="validate" value="masukan foto"><br>
	 		</div>


	 		<div class="from-group col-md-offset-4 col-md-4">
	 		<center><table border="0"></center>
	 			<tr><td><input type="submit" value="Upload" name="bukti" class="btn btn-primary col-md-12"></td>
	 				</tr>
	 			</table>

	 		</div>
		</form>
    </div>

 