<html>
<head>
	<title>Indobisa - Pendaftaran</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/creative.css" rel="stylesheet">
    <link rel="stylesheet"  href="css/animate.css">
    <link rel="stylesheet"  href="css/style.css">
</head>
<body bgcolor="#F05F40">

<div class="container-fluid" style="background-color: black">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php#page-top">IndoBisa</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="index.php#page-top">Beranda</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#program">Program</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#dokumentasi">Dokumentasi</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#tentangkami">Tentang Kami</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#hubungikami">Hubungi Kami</a>
                    </li>
                    <li>
                        <a style="background-color:transparent;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Masuk</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>


<?php
include "koneksi.php";

//$tabel = "SELECT * FROM user";
//$query = mysqli_query($mysql, $tabel);
if (isset($_POST['Daftar'])) 

{


$nama = $_POST['nama'];
$username = $_POST['username'];
$password = md5($_POST['password']);
$repassword = md5($_POST['repassword']);
$email = $_POST['email'];
$telepon = $_POST['no_tlp'];
$id = date('Ymd').substr(($nama),0,2).substr($telepon,0,2);

$cekuser = mysqli_query($mysql, "SELECT * FROM user WHERE username= '$username'");

 if(mysqli_num_rows($cekuser) > 0) 
 {
	echo "Username sudah terdaftar. Jika ingin beralih ke akun lain, silahkan lakukan pergantian jenis akun<br>";
	echo "Klik disini untuk kembali <a href='daftar.php'>Daftar</a> atau <a href='login.php'>Login</a>";
  	header("refresh:3;http://localhost/indobisa/login.php");

 }

 else
 {
 	 if($password!=$repassword ) {
       echo "<div align='center'>Password tidak sama<br>";
       echo "Pindah ke Sign Up dalam 3 detik ...";
   	   header("refresh:3;http://localhost/indobisa/daftar.php");
     }

     else
     {	
		$data= "INSERT INTO user (id_user,nama,username,password,email,no_tlp) VALUES ('$id','$nama','$username','$password','$email','$telepon')";
		$simpan = mysqli_query($mysql, $data);

		if($simpan) {
		echo ' 	<div class="container">
        <div class="row">
            <div class="text-center" >
           <!--  	<img src="img/logo.jpeg"> -->
           		<br>
            	<h2 class="section-heading">Pendaftaran Sukses !</h2>
               	<hr class="primary">
            </div>
        </div>';	
		echo "<center>Silahkan menuju kehalaman login <a href='login.php'>Sign In</a></center><br>";
		echo "<center>Atau otomatis berpindah ke Login dalam 3 detik</center>";
		header("refresh:3;http://localhost/indobisa/login.php");

	 	} 
	 	else 
	 	{
		echo "Proses Gagal!";
		}
	}
}

}

else
{
	 echo "<script language='javascript'>alert('Maaf, tidak bisa daftar'); document.location='daftar_donatur.php'</script>";

}

?>
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Masuk IndoBisa</h4>
        </div>
        <div class="modal-body">
            <form action="" method="POST" class="responsive" style="">
                <div class="row">
                    <div class="imgcontainer row text-center  col-sm-5">
                        <img src="img_avatar2.png" alt="Avatar" height="150px" class="avatar">
                    </div>
                    <div class="row  col-sm-5" align="center" style="padding-left: 50px">
                        <div class="form-group">
                            <div class="row" style="padding-bottom: 20px">
                                <input type="text" class="form-control" placeholder="Enter Username" name="username" required>    
                            </div>
                            <div class="row" style="padding-bottom: 20px">
                                <input type="password" class="form-control" placeholder="Enter Password" name="password" required>    
                            </div>
                            <div class="row">
                                <div class="col-sm-5"></div>
                                <button class="btn btn-primary col-sm-7" type="submit" name="login">Masuk</button> 
                            <div class="row">
                               Belum punya akun ?<a href = "http://localhost/indobisa/daftar.php"> Daftar</a>
                            </div> 
                            <div class="row">
                               Lupa Password ?<a href = "http://localhost/indobisa/forgot_password.<?php  ?>"> Lupa Password</a>
                            </div>   
                            </div>            
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="js/creative.min.js"></script>

</body>
</html>