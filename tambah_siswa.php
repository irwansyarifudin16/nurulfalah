<?php
include "session_admin.php";  
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Tambah Siswa</title>  
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/creative.css" rel="stylesheet">
    <link rel="stylesheet"  href="css/animate.css">
    <link rel="stylesheet"  href="css/style.css">
  
</head>

<body>

<div class="container-fluid" style="background-color: black">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php#page-top">IndoBisa</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="index.php#page-top">Beranda</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#program">Program</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#dokumentasi">Dokumentasi</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#tentangkami">Tentang Kami</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#hubungikami">Hubungi Kami</a>
                    </li>
                    <li>
                        <a style="background-color:transparent;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Masuk</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
<br>  
<p align="center"><a href="home_admin.php" class="btn waves-effect waves-light col s12">Kembali ke home</a></p>
<div class="container">
        <div class="row">
            <div class="text-center" >
              <h2 class="section-heading">Tambah Data Siswa</h2>
                <hr class="primary">
            </div>
</div>
  <form method="post" action="tambah_siswa_proses.php" enctype="multipart/form-data">

      <div class="from-group col-md-offset-4 col-md-4">
        <label class="label-username" name="username">
          Nomor Induk Siswa (NIS)
          <br>
        </label>
        <br>
        <input type="number" name="NIS" class="form-control" placeholder="NIS" required><br>
      </div>
    
      <div class="from-group col-md-offset-4 col-md-4">
        <label class="label-username" name="username">
          Nama Siswa
          <br>
        </label>
        <br>
        <input type="text" name="nama_siswa" class="form-control" placeholder="nama siswa" required>
      </div>

      <div class="from-group col-md-offset-4 col-md-4">
      <br>
        <label class="label-username" name="username">
          Tanggal Lahir
          <br>
        </label>
        <br>
        <select name="tgl">
        <?php
        for($i=1; $i<=31; $i++)
        {
        echo "<option value='".$i."'>".$i."</option>";
        }
        ?>
        </select>
        <select name="bln">
        <?php
        for($i=1; $i<=12; $i++)
        {
        echo "<option value='".$i."'>".$i."</option>";
        }
        ?>
        </select>
        <select name="thn">
        <?php
        for($i=2000; $i<=2020; $i++)
        {
        echo "<option value='".$i."'>".$i."</option>";
        }
        ?>
        </select> 
      </div>

      <div class="from-group col-md-offset-4 col-md-4">
      <br>
        <label class="label-username" name="kelas">
          Kelas
          <br>
        </label>
        <br>
        <select name="kelas" required>
        <?php
        for($i=1; $i<=6; $i++)
        {
        echo "<option value='".$i."'>".$i."</option>";
        }
        ?>
        </select> 
      </div>

      <div class="from-group col-md-offset-4 col-md-4">
      <br>
        <label class="label-username" name="angkatan">
          Angkatan
          <br>
        </label>
        <br>
        <select name="angkatan" required>
        <?php
        for($i=2010; $i<=2020; $i++)
        {
        echo "<option value='".$i."'>".$i."</option>";
        }
        ?>
        </select> 
      </div>


      <div class="from-group col-md-offset-4 col-md-4">
      <br>
        <label class="label-email" name="nama_orangtua">
          Nama Orang Tua
        </label>
        <br>
        <input type="text" name="nama_orangtua" class="form-control" placeholder="keterangan" required>
      </div>

      <div class="from-group col-md-offset-4 col-md-4">
      <br>
        <label class="label-email" name="no_telp">
          Nomor Telepon
        </label>
        <br>
        <input type="number" name="no_telp" class="form-control" placeholder="keterangan">
      </div>

      <div class="from-group col-md-offset-4 col-md-4">
      <br>
        <label class="label-email" name="alamat">
          Alamat
        </label>
        <br>
        <input type="text" name="alamat" class="form-control" placeholder="keterangan"><br>
      </div>

      <div class="from-group col-md-offset-4 col-md-4">
      <label class="label-email" name="file">
      Upload foto Siswa
      </label>  
      <input type="file" name="file" class="validate" value="masukan foto"><br>
      </div>

      <div class="from-group col-md-offset-4 col-md-4">
      <input type="submit" name="tambah" value="tambah" class="btn waves-effect waves-light col s12">
      <br>
      </div>
    </form>
    </div>

<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Masuk IndoBisa</h4>
        </div>
        <div class="modal-body">
            <form action="" method="POST" class="responsive" style="">
                <div class="row">
                    <div class="imgcontainer row text-center  col-sm-5">
                        <img src="img_avatar2.png" alt="Avatar" height="150px" class="avatar">
                    </div>
                    <div class="row  col-sm-5" align="center" style="padding-left: 50px">
                        <div class="form-group">
                            <div class="row" style="padding-bottom: 20px">
                                <input type="text" class="form-control" placeholder="Enter Username" name="username" required>    
                            </div>
                            <div class="row" style="padding-bottom: 20px">
                                <input type="password" class="form-control" placeholder="Enter Password" name="password" required>    
                            </div>
                            <div class="row">
                                <div class="col-sm-5"></div>
                                <button class="btn btn-primary col-sm-7" type="submit" name="login">Masuk</button> 
                            <div class="row">
                            <br><br>
                               Belum punya akun ?<a href = "http://localhost/indobisa/daftar.php"> Daftar</a>
                            </div> 
                            <div class="row">
                               Lupa Password ?<a href = "http://localhost/indobisa/daftar.php"> Lupa Password</a>
                            </div>                                 
                            </div>            
                        </div>
                    </div>
                </div>
            </form>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

 <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/creative.min.js"></script>

</body>

</html>