<?php 
include "session_admin.php";
?>

<html>
<head>
	<title>Indobisa - Signup</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/creative.css" rel="stylesheet">
    <link rel="stylesheet"  href="css/animate.css">
    <link rel="stylesheet"  href="css/style.css">
</head>
<body bgcolor="#F05F40">
<div class="container-fluid" style="background-color: black">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php#page-top">IndoBisa</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="index.php#page-top">Beranda</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#program">Program</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#dokumentasi">Dokumentasi</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#tentangkami">Tentang Kami</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#hubungikami">Hubungi Kami</a>
                    </li>
                    <li>
                        <a style="background-color:transparent;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Masuk</a>
                    </li>
                </ul>
            </div>
            </div>
 	<div class="container">
        <div class="row">
            <div class="text-center" >
           <!--  	<img src="img/logo.jpeg"> -->
           		<br>
            	<h2 class="section-heading">DAFTAR JADI ADMIN</h2>
               	<hr class="primary">
            </div>
        </div>
      	<form action="proses_daftar_admin.php" method="POST">
 		
	 		<div class="from-group col-md-offset-4 col-md-4">
	 			<label class="label-username" name="username">
	 				Nama Lengkap
	 				<br>
	 			</label>
	 			<br>
	 			<input type="text" name="nama_admin" class="form-control" placeholder="nama" required>
	 		</div>

	 		<div class="from-group col-md-offset-4 col-md-4">
	 			<label class="label-username" name="username">
	 				Username
	 				<br>
	 			</label>
	 			<br>
	 			<input type="text" name="username_admin" class="form-control" placeholder="Username" required>
	 		</div>

	 		<br>
	 		<div class="from-group col-md-offset-4 col-md-4">
	 			<label class="label-email" name="email">
	 				Email
	 			</label>
	 			<br>
	 			<input type="text" name="email_admin" class="form-control" placeholder="Email">
	 		</div>

	 		<br>
	 		<div class="from-group col-md-offset-4 col-md-4">
	 			<label class="label-password" name="password">
	 				Password
	 			</label>
	 			<input type="password" name="password_admin" class="form-control" placeholder="Password" required>
	 		</div>
            
	 		<br>
	 		<div class="from-group col-md-offset-4 col-md-4">
	 			<label class="label-repassword" name="repassword" required>
	 				Re-Password
	 			</label>
	 			<input type="password" name="repassword_admin" class="form-control" placeholder="Re-Password"><br><br>
	 				 			<center><table border="0"></center>
	 			<tr><td><input type="submit" value="Daftar" name="Daftar" class="btn btn-primary col-md-12"></td>
	 			<td>&nbsp</td>
	 				<td><input type="reset" value="Reset" name="Reset" class="btn btn-primary col-md-12"></td>
	 				</tr>
	 			</table>

	 		</div>
		</form>
    </div>

 