<!DOCTYPE html>
<html lang="en">
<head>
    <title>Indobisa - Donasi</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/creative.css" rel="stylesheet">
    <link rel="stylesheet"  href="css/animate.css">
    <link rel="stylesheet"  href="css/style.css">
</head>
<body bgcolor="#F05F40">
<div class="container-fluid" style="background-color: black">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php#page-top">IndoBisa</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="index.php#page-top">Beranda</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#program">Program</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#dokumentasi">Dokumentasi</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#tentangkami">Tentang Kami</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#hubungikami">Hubungi Kami</a>
                    </li>
                    <li>
                        <a style="background-color:transparent;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Masuk</a>
                    </li>
                </ul>
            </div>

            <!-- /.navbar-collapse -->

            </div>
<br><br>
    <div class="container">
        <div class="row">
            <div class="text-center" >
           <!--     <img src="img/logo.jpeg"> -->
                <br>

<?php

include "koneksi.php";


if($_POST['lanjut'])

{


            $tabel = "SELECT * FROM daftar_sumbangan_nonuser";
            $query = mysqli_query($mysql, $tabel);

            $ekstensi_diperbolehkan = array('png','jpg');
            $nama = $_FILES['file']['name'];
            $x = explode('.', $nama);
            $ekstensi = strtolower(end($x));
            $ukuran = $_FILES['file']['size'];
            $file_tmp = $_FILES['file']['tmp_name'];
            $format = 'foto';   


            $nama_nonuser = $_POST['nama_nonuser'];
            $nama_barang_nonuser = $_POST['nama_barang_nonuser'];
            $email_barang_nonuser = $_POST['email_barang_nonuser'];
            $no_telp_barang_nonuser= $_POST['no_telp_barang_nonuser'];
            $jenis_barang_nonuser = $_POST['jenis_barang_nonuser'];
            $jumlah_barang_nonuser = $_POST['jumlah_barang_nonuser'];
            $id_barang_nonuser = date('YmdGis').substr(($nama_barang_nonuser),0,2).substr(($nama_nonuser),0,2);

            $cekuser = mysqli_query( $mysql, "SELECT *from daftar_sumbangan_nonuser where id_barang_nonuser='$id_barang_nonuser'");

            if(mysqli_num_rows($cekuser) > 0) 
            {
                        echo "Klik disini untuk kembali menyumbang <a href='sumbangan_awal.php'>Sumbang Barang</a>";
                        header("refresh:3;http://localhost/indobisa/sumbangan_awal.php");
            } 


            else
            {

            if(in_array($ekstensi, $ekstensi_diperbolehkan) === true)

            {
                if($ukuran < 1044070)

                {          
                    move_uploaded_file($file_tmp, 'file/'.$nama);

                    $query = mysqli_query($mysql, "INSERT INTO daftar_sumbangan_nonuser (tgl_masuk_nonuser,id_barang_nonuser,nama_nonuser,nama_barang_nonuser,jenis_barang_nonuser,jumlah_barang_nonuser,email_barang_nonuser,no_telp_barang_nonuser,foto_nonuser) VALUES (now(), '$id_barang_nonuser', '$nama_nonuser', '$nama_barang_nonuser', '$jenis_barang_nonuser', '$jumlah_barang_nonuser', '$email_barang_nonuser' , '$no_telp_barang_nonuser','$nama')");
                    if($query)
                    {
                        echo '<h2 style="padding-top: 50px;" class="section-heading">Terima kasih ';echo '<i><b>'.$nama_nonuser.'</b></i><br>';echo 'telah memberikan sumbangan barang <br> dengan nomor transaksi '.'<b>'.$id_barang_nonuser.'</b>.'.'</h2>';  
                        echo '<h3 style="padding-top: 50px;" class="section-heading">Barang yang disumbangkan '.$nama_nonuser.' diantaranya :';
                        echo '<h3>- Nama Barang : '.$nama_barang_nonuser;
                        echo '<h3>- Jumlah Barang : '.$jumlah_barang_nonuser;
                    }

                    else
                    {
                        echo '<h2 style="padding-top: 50px;" class="section-heading">Maaf, Tidak Ada Transaksi</h2>';

                    }                    


                }

                else

                {
                    echo 'UKURAN FILE TERLALU BESAR';
                }
            }


            else
            {
                    echo "<script language='javascript'>alert('Maaf, gagal berdonasi  !'); document.location='sumbangan_awal.php'</script>";
            }
               
            }

}

else
{
                    echo "<script language='javascript'>alert('Mau ngapain ?. Donasi dulu.'); document.location='sumbangan_awal.php'</script>";

}
?>
    
    </div>
    <center><br><p class="section-heading"><i>Konfirmasi segera ke Ibu Laila.</i></p></center>
    <form action="sumbangan_awal.php" method="POST">
    <input  type="submit" value="Sumbang lagi" name="konfirmasi" class="btn btn-primary col-md-4 col-sm-4 col-md-offset-4"><br><br><br>
    </form>

    </div>
    </body>
    </html>





