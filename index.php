<?php
//include 'session.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="@radenisum @cepnana @abirachmanwasril">
    <meta name="keywords" content="IndoBisa, Indonesia Bisa, Orang tua asuh, donatur, donasi, relawan, anak asuh">

    <title>Nurul Falah Muncul</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/creative.min.css" rel="stylesheet">
    <link rel="stylesheet"  href="css/animate.css">
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/script.js"></script>


</head>

<body id="page-top">

    <nav style="background-color: white" id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span style="color:black" class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top" style="color:black">MI Nurul Falah Muncul</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="#page-top" style="color:black">Beranda</a>
                    </li>


                    <li>
                        <a class="page-scroll" href="#program" style="color:black">Program</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#dokumentasi" style="color:black">Dokumentasi</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#tentangkami" style="color:black">Tentang Kami</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#hubungikami" style="color:black">Hubungi Kami</a>
                    </li>
                    <li>
                        <a style="background-color:transparent;color:black" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="">Masuk</a>
                    </li>
                </ul>
            </div>

        </div>

    </nav>

<header>

  <div class="w3-content w3-section" >
  <img class="mySlides" src="img/f.jpg" height="700px" width="100%">
  <img class="mySlides" src="img/lari1.jpg" height="700px" width="100%">
  <img class="mySlides" src="img/a.jpg" height="700px" width="100%" >
</div>

<script>
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 2000); // Change image every 2 seconds
}
</script>

<div class="header-content">
            <div class="header-content-inner">
                <h1 id="homeHeading">"Sedikit perhatian anda </br> berarti besar untuk mereka"</h1>
                <hr>
                <p><b>Mari bantu wujudkan harapan mereka agar terciptanya generasi Indonesia cerdas. Indonesia pasti Bisa !</b></p>
                <a href="donasi_awal.php" class="btn btn-primary btn-xl page-scroll">Donasi Sekarang</a>
            </div>
        </div>
    </header>
     <section id="program">
        <div class="container">
            <div class="row">
                <div class="text-center">
                    <h2 class="section-heading">PROGRAM</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row"> </div>
            </div>
                <div class="text-center">
                    <div class="col-md-8 col-md-offset-2">
                        <i class="fa fa-4x fa-diamond text-primary sr-icons"></i><br>
                        Yuk bantu wujudkan cita-cita mereka untuk Indonesia menjadi lebih baik!:)<br><br>
                       <a href="daftar_donatur.php"><button class="btn btn-primary col-md-offset-0.99 col-md-0.99." type="submit" name="daftar">Jadi Donatur</button></a>
                       <a href="daftar_kakakasuh.php"><button class="btn btn-primary col-md-offset-0.99 col-md-0.99." type="submit" name="daftar">Jadi Kakak Asuh</button></a>
                       <a href="daftar_volunteer.php"><button class="btn btn-primary col-md-offset-0.99 col-md-0.99." type="submit" name="daftar">Jadi Volunteer</button></a>

                </div>
             </div>
          </div>
        </div>
    </section>

    <section class="no-padding" id="dokumentasi">
    <div class="container">
            <div class="row">
                <div class="text-center">
                 <br>
                    <h2 class="section-heading">DOKUMENTASI</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <br>
        <div class="col-lg-4 col-sm-6">
                    <a href="img/renang.jpg" class="portfolio-box">
                        <img src="img/renang.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Renang bersama siswa
                                </div>
                                <div class="project-name">
                                    Kegiatan ini untuk mengisi libur bersama siswa/i Nurul Falah
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                </div>

                <div class="col-lg-4 col-sm-6">
                </div>
            </div>
        </div>
    </section>
   <section class="bg-primary" id="tentangkami">
        <div style="padding-bottom: 100x;" class="container">
            <div class="row">
                <div style="" class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">TENTANG KAMI</h2>
                    <p class="section-heading">Program donatur, Orang Tua Asuh, dan Volunteer merupakan suatu rintisan program yang sengaja dibentuk oleh MI Nurul Falah Muncul Tangerang Selatan.</p>
                    <button class="btn btn-primary col-md-offset-0.99 col-md-0.99."  type="button" name="daftar" data-toggle="modal" data-target="#proGram" style="">Program</button></a>
                    <button class="btn btn-primary col-md-offset-0.99 col-md-0.99."  type="button" name="daftar" data-toggle="modal" data-target="#visiMisi" style="">Visi Misi</button></a>
                    <button class="btn btn-primary col-md-offset-0.99 col-md-0.99."  type="button" name="daftar" data-toggle="modal" data-target="#pengembang" style="">Pengembang</button></a><br>
                    <a href="profil.php" blank=""><button class="btn btn-primary col-md-offset-0.99 col-md-0.99."  type="button" name="daftar" style="">Profil Sekolah</button></a><br>
      </section>

    <section id="hubungikami">
        <div class="bounce animated">
            <div class="row">
                <div class="text-center">
                    <h2 class="section-heading">Hubungi Kami</h2>
                    <hr class="primary">
                    <div class="col-md-offset-1 col-md-3">
                        <div>
                        <br>
                        <br>
                        <br>
                            <p style="">Alamat</p>
                        <p>Jl.Buaran Gardu Rt 004/001 No.87 Serpong - Tangerang Selatan</p>    
                        </div>
                        <div class=" text-center">
                            <i class="fa fa-phone fa-2x sr-contact"></i>
                            <p>Ibu Laila - 085695544862</p>    
                        </div>
                        <div class=" text-center">
                            <i class="fa fa-envelope-o fa-2x sr-contact"></i>
                            <p><a href="mailto:your-email@your-domasin.com">sukses@minurulfalah.sch.id</a></p>      
                        </div>
                    </div>
                    <br>
                    <div id="map" class="col-md-offset-1 col-md-6">
                      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.4379674138368!2d106.68492621476969!3d-6.337273695413866!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69e4c2e2657aed%3A0x9a7e51a472687f9!2sSerpong+Terrace!5e0!3m2!1sen!2sid!4v1481211706104" width="82%" height="34%" frameborder="0" style="border:0; height: 300px;" allowfullscreen></iframe>
                    </div>    
                </div>
            </div>
                <!-- Footer -->

        <div class="wrapper row4">
          <footer id="footer" class="clear">
            <br>
            <h5 style="text-align: center; padding:78px;">Copyright &copy; 2016 - All Rights Reserved - <a href="#">MI Nurul Falah Muncul</a> - <a href="sdank.php" target = '_blank'>Syarat dan Ketentuan Donatur</a></h5>
          </footer>
        </div>


        </div>
    </section>


     <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Small Modal</button> -->

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Masuk sebagai Member</h4>
           <form action="proses_login2.php" method="POST" class="responsive" style="">
                <div class="row">
                    <div class="imgcontainer row text-center  col-sm-5">
                        <img src="img_avatar2.png" alt="Avatar" height="150px" class="avatar">
                    </div>
                    <div class="row  col-sm-5" align="center" style="padding-left: 50px">
                        <div class="form-group">
                            <div class="row" style="padding-bottom: 20px">
                                <input type="text" class="form-control" placeholder="Enter Username" name="username" required>    
                            </div>
                            <div class="row" style="padding-bottom: 20px">
                                <input type="password" class="form-control" placeholder="Enter Password" name="password" required>    
                            </div>
                            <div class="row">
                                <div class="col-sm-5"></div>
                                <button class="btn btn-primary col-sm-7" type="submit" name="login">Masuk</button> 
                            <div class="row">
                            <br><br>
                               Belum punya akun ?<a href = "daftar.php"> Daftar</a>
                            </div> 
                            <div class="row">
                               Lupa Password ?<a href = "forgot_password.php"> Lupa Password</a>
                            </div>                                   
                            </div>            
                        </div>
           
        </div>
        <div class="modal-body">
                    </div>
                </div>
              </form>
            <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

  <div class="modal fade" id="visiMisi" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
      <h2 align="center">Visi</h2>
        <div class="modal-header">
        <p align="center" class="section-heading">Terbentuknya generasi qur’ani yang berakhlakul karimah, unggul dalam prestasi dan berwawasan global khususnya di Tanggerang Selatan.</p>
        </div>
        <h2 align="center">Misi</h2>
        <div class="modal-body">
        <p align="justify" class="section-heading">
        1.    Menanamkan nilai-nilai aqidah, syariah dan akhlakl karimah sebagai basis bagi segenap aspek kehidupan dengan mengembangkan program tahfidz, tartil dan tahsinul qur’an serta praktek Ibadah.<br><br>
        2.  Mengembangkan proses pembelajaran yang tidak sekedar menstransfer pengetahuan dan keterampilan, tetapi juga motivasi dan pengembangan kualitas kepribadian siswa dengan mengacu kepada prinsip dan teori psikologis dengan penerapan strategi pembelajaran paikem.<br><br>
        3.  Meningkatkan daya saing lulusan serta meningkatkan kualifikasi dan profesionalisme bagi tenaga pendidik dan tenaga kependidikan lainnya melalui pelatihan-pelatihan.<br><br>
        4.  Membudayakan peran serta masyarakat dalam penyelenggaraan pendidikan berdasarkan prinsip otonomi dan manajemen berbasis madrasah serta peguatan terhadap fungsi dan peran komite.
        </p>
            <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="proGram" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
      <h2 align="center">Tentang Program</h2><hr>

      <p align="center" class="section-heading">Program donatur, Orang Tua Asuh, dan Volunteer merupakan suatu rintisan program yang sengaja dibentuk oleh MI Nurul Falah Muncul Tangerang Selatan.</p>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="pengembang" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
      <h2 align="center">Tentang Pengembang</h2><hr>
      <center><table class="section-heading" width="50px" border="0" >
          <tr><td align="center">Irwan</td><td align="center">Irfan</td><td align="center">Ade</td>
          </tr>
          <tr><td align="center">    <a href="#" style="float: left;" class="img-foto container-fluid col-md-3"  ><img src="img/irwan.jpg" width="90px" height="90px" alt=""></a></td>

                <td align="center"><a href="#" style="float: left;" class="img-foto container-fluid col-md-3"  ><img src="img/irfan.JPG" width="90px" height="90px" alt=""></a></td>

                    <td align="center"><a href="#" style="float: left;" class="img-foto container-fluid col-md-3"  ><img src="img/ade.jpeg" width="90px" height="90x" alt=""></a></td>
          </tr>
           <tr><td align="center"><h5 style="color: black;">Back-End</h5></td><td align="center"><h5 style="color: black;">Front-End</h5></td><td align="center"><h5 style="color: black;">Back-End</h5></td>
          </tr>
      </table>
      </center>
      </div>
        
            <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>


    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="js/creative.min.js"></script>

</body>

</html>
