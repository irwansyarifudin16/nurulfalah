<?php
include "koneksi.php";

if (isset($_GET['id_kegiatan'])) { 
$id_kegiatan = $_GET['id_kegiatan'];
} else {
die ("Error. No Id Selected! ");
}

?>

<?php 
include "proses_update_kegiatan.php";
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Update Kegiatan</title>
  
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/creative.css" rel="stylesheet">
    <link rel="stylesheet"  href="css/animate.css">
    <link rel="stylesheet"  href="css/style.css">
    
</head>

<body>
<div class="container-fluid" style="background-color: black">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php#page-top">IndoBisa</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="home_admin.php#page-top">Beranda</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="home_admin.php#program">Program</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="home_admin.php#dokumentasi">Dokumentasi</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="home_admin.php#tentangkami">Tentang Kami</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="home_admino.php#hubungikami">Hubungi Kami</a>
                    </li>
                    <li>
                        <a style="background-color:transparent;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Masuk</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
<br>
<p align="center"><a href="home_admin.php" class="btn waves-effect waves-light col s12">Kembali ke home</a></p>

<div class="container">
        <div class="row">
            <div class="text-center" >
           <!--   <img src="img/logo.jpeg"> -->
              <br>
              <h2 class="section-heading">PROGRAM</h2>
                <hr class="primary">
            </div>
        </div>
  <form method="post" action="" enctype="multipart/form-data">
    
      <div class="from-group col-md-offset-4 col-md-4">
        <label class="label-username" name="username">
          Nama Kegiatan
          <br>
        </label>
        <br>
        <input type="text" name="nama_kegiatan" class="form-control" placeholder="<?php echo $nama_kegiatan; ?>" required>
      </div>
      <div class="from-group col-md-offset-4 col-md-4">
      <br>
        <label class="label-username" name="username">
          Tanggal Kegiatan
          <br>
        </label>
        <br>
        <select name="tgl">
        <?php
        for($i=1; $i<=31; $i++)
        {
        echo "<option value='".$i."'>".$i."</option>";
        }
        ?>
        </select>
        <select name="bln">
        <?php
        for($i=1; $i<=12; $i++)
        {
        echo "<option value='".$i."'>".$i."</option>";
        }
        ?>
        </select>
        <select name="thn">
        <?php
        for($i=1980; $i<=2016; $i++)
        {
        echo "<option value='".$i."'>".$i."</option>";
        }
        ?>
        </select> 
      </div>
      <div class="from-group col-md-offset-4 col-md-4">
      <br>
        <label class="label-email" name="email">
          Keterangan
        </label>
        <br>
        <input type="text" name="keterangan" class="form-control" placeholder="<?php echo $keterangan; ?>" required><br>
      </div>

      
      <div class="from-group col-md-offset-4 col-md-4">
      <br>
        <label class="label-username" name="username">
          Kategori
          <br>
        </label>
        <br>
        <select name="kategori" required>
        <option value="prestasi">Prestasi</option>
        <option value="esktrakulikuler">Esktrakulikuler</option>
        <option value="rutinitas">Rutinitas</option>
        </select> 
      </div>

      <div class="from-group col-md-offset-4 col-md-4">
      <label class="label-email" name="email">
      Upload foto kegiatan
      </label>  
      <br>
      <img src="<?php echo "file/".$foto_kegiatan; ?>" width="120px" height="80px" border="1" align="center">
      <input type="file" name="file" class="validate" value="masukan foto"><br>
      </div>

      <div class="from-group col-md-offset-4 col-md-4">
      <input type="submit" name="update" value="update kegiatan" class="btn waves-effect waves-light col s12"><br>
      </div>
    </form>
</body>

</html>