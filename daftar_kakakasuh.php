<?php
    include "koneksi.php"
?>

<?php
if(isset($_SESSION['login_user']))
{

header("location: home.php");

}

?>

<html>
<head>
	<title>Indobisa - Signup</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/creative.css" rel="stylesheet">
    <link rel="stylesheet"  href="css/animate.css">
    <link rel="stylesheet"  href="css/style.css">
</head>
<body bgcolor="#F05F40">

<div class="container-fluid" style="background-color: black">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php#page-top">IndoBisa</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="index.php#page-top">Beranda</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#program">Program</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#dokumentasi">Dokumentasi</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#tentangkami">Tentang Kami</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#hubungikami">Hubungi Kami</a>
                    </li>
                    <li>
                        <a style="background-color:transparent;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Masuk</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
 	<div class="container">
        <div class="row">
            <div class="text-center" >
           <!--  	<img src="img/logo.jpeg"> -->
           		<br>
            	<h2 class="section-heading">DAFTAR JADI KAKAK ASUH</h2>
               	<hr class="primary">
            </div>
        </div>
      	<form action="proses_daftar_kakakasuh.php" method="POST">
 		
	 		<div class="from-group col-md-offset-4 col-md-4">
	 			<label class="label-username" name="username">
	 				Nama Lengkap
	 				<br>
	 			</label>
	 			<br>
	 			<input type="text" name="nama" class="form-control" placeholder="nama" required><br>
	 		</div>

	 		<div class="from-group col-md-offset-4 col-md-4">
	 			<label class="label-username" name="username">
	 				Username
	 				<br>
	 			</label>
	 			<br>
	 			<input type="text" name="username" class="form-control" placeholder="Username" required><br>
	 		</div>
	 		<br>
	 		<div class="from-group col-md-offset-4 col-md-4">
	 			<label class="label-email" name="email">
	 				Email
	 			</label>
	 			<br>
	 			<input type="email" name="email" class="form-control" placeholder="Email"><br>
	 		</div>
            <br>
            <div class="from-group col-md-offset-4 col-md-4">
                <label class="label-email" name="email">
                    No Telepon
                </label>
                <br>
                <input type="number" name="no_telp" class="form-control" placeholder="isi nomer telepon yang masih aktif"><br>
            </div>
            <br>
            
            <!--
            <div class="from-group col-md-offset-4 col-md-4">
                <label class="label-email" name="asal_instansi">
                    Asal Instansi
                </label>
                <br>
                <input type="text" name="asal_instansi" class="form-control" placeholder="asal instansi kamu"><br>
            </div       
            <br>
            <div class="from-group col-md-offset-4 col-md-4">
                <label class="label-email" name="pekerjaan">
                    Pekerjaan 
                </label>
                <br>	
                <input type="text" name="pekerjaan" class="form-control" placeholder="contoh : matematika / IPA / IPS / Bahasa Indonesia / Bahasa Inggris"><br>
            </div       
            <br>
            <div class="from-group col-md-offset-4 col-md-4">
                <label class="label-email" name="pekerjaan">
            		Lama Mengasuh
                </label>
                <br>	
                <input type="radio" name="asuh"  value="satu_semester">&nbsp 1 semester<br>
                <input type="radio" name="asuh"  value="setahun">&nbsp 1 tahun<br>
                <input type="radio" name="asuh"  value="dua_tahun">&nbsp 2 tahun<br>
                <input type="radio" name="asuh"  value="dua_tahun">&nbsp Sampai siswa lulus<br><br>

            </div       
            <br> -->

	 		<div class="from-group col-md-offset-4 col-md-4">
	 			<label class="label-password" name="password">
	 				Password
	 			</label>
	 			<input type="password" name="password" class="form-control" placeholder="Password" required><br>
	 		</div>
	 		<br>
	 		<div class="from-group col-md-offset-4 col-md-4">
	 			<label class="label-repassword" name="repassword" required>
	 				Konfirmasi Password
	 			</label>
	 			<input type="password" name="repassword" class="form-control" placeholder="Re-Password"><br>
            </div>

            <div class="from-group col-md-offset-4 col-md-4">
                <input type="checkbox" name="cek" required> Saya sudah membaca <a href="sdank.php">syarat dan ketentuan.</a>       
            </div>

            <div class="from-group col-md-offset-4 col-md-4"><br>
                <center><table border="0"></center>
                <tr><td><input type="submit" value="Daftar" name="Daftar" class="btn btn-primary col-md-12"></td>
                <td>&nbsp</td>
                    <td><input type="reset" value="Reset" name="Reset" class="btn btn-primary col-md-12"></td>
                    </tr>
                </table>
            </div>

		</form>
    </div>
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Masuk IndoBisa</h4>
        </div>
        <div class="modal-body">
            <form action="" method="POST" class="responsive" style="">
                <div class="row">
                    <div class="imgcontainer row text-center  col-sm-5">
                        <img src="img_avatar2.png" alt="Avatar" height="150px" class="avatar">
                    </div>
                    <div class="row  col-sm-5" align="center" style="padding-left: 50px">
                        <div class="form-group">
                            <div class="row" style="padding-bottom: 20px">
                                <input type="text" class="form-control" placeholder="Enter Username" name="username" required>    
                            </div>
                            <div class="row" style="padding-bottom: 20px">
                                <input type="password" class="form-control" placeholder="Enter Password" name="password" required>    
                            </div>
                            <div class="row">
                                <div class="col-sm-5"></div>
                                <button class="btn btn-primary col-sm-7" type="submit" name="login">Masuk</button> 
                            <div class="row">
                               Belum punya akun ?<a href = "http://localhost/indobisa/daftar.php"> Daftar</a>
                            </div> 
                            <div class="row">
                               Lupa Password ?<a href = "http://localhost/indobisa/forgot_password.<?php  ?>"> Lupa Password</a>
                            </div>   
                            </div>            
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>



    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="js/creative.min.js"></script>


 


