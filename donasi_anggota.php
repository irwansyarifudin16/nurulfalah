<?php 
include "session.php";  
       
?>

<html>
<head>
	<title>Indobisa - Donasi</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/creative.css" rel="stylesheet">
    <link rel="stylesheet"  href="css/animate.css">
    <link rel="stylesheet"  href="css/style.css">
</head>
<body bgcolor="#F05F40">
<div class="container-fluid" style="background-color: black">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php#page-top">IndoBisa</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="index.php#page-top">Beranda</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#program">Program</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#dokumentasi">Dokumentasi</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#tentangkami">Tentang Kami</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#hubungikami">Hubungi Kami</a>
                    </li>
                    <li>
                        <a style="background-color:transparent;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Masuk</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
 	<div class="container">
        <div class="row">
            <div class="text-center">
           <!--  	<img src="img/logo.jpeg"> -->
           		<br>
            	<h2 class="section-heading">Ayo Donasi</h2>
               	<hr class="primary">
            </div>
        </div>
      	<form action="proses_donasi_pertama_anggota.php" method="POST">
 		
	 		<div class="from-group col-md-offset-4 col-md-4">
	 			<label class="label-username" name="username">
	 				Masukan nominal donasi.
	 				<i>Donasi</i>
	 			</label>
	 			<br>
	 			<input type="number" name="jml_transaksi" class="form-control" placeholder="nominal donasi" min=14999 required>
	 		</div>

	 		<div class="from-group col-md-offset-4 col-md-4"><br>
	 			<label class="label-username" name="username">
	 				Nama Bank Rekening	
	 				<br>
	 			</label>
	 			<br>
	 			<select type="text" name="nama_bank" class="form-control" placeholder="nama_bank" required>
				<option value="BRI SYARIAH">BRI SYARIAH</option>
				<option value="BRI">BRI</option>
				<option value="BNI SYARIAH">BNI SYARIAH</option>
				<option value="MANDIRI SYARIAH">MANDIRI SYARIAH</option>
				<option value="MANDIRI">MANDIRI</option>
				<option value="BCA">BCA</option>
				<option value="BANK DKI">BANK DKI</option>
				<option value="PERMATA BANK">PERMATA BANK</option>


	 			<br>
	 		</div>
	 		<br>
	 		<div class="from-group col-md-offset-4 col-md-4">
	 			<br>
	 			<input type="number" name="no_rek" class="form-control" placeholder="isi nomor rekening" value="isi nomor rekening" required>
	 		</div>	 
	 		<div class="from-group col-md-offset-4 col-md-4">
	 		<br><br>
	 			<center><table border="0"></center>
	 			<tr><td><input type="submit" value="Lanjut" name="lanjut" class="btn btn-primary col-md-12"></td>
	 			<td>&nbsp</td>
	 				<td><input type="reset" value="Batal" name="Reset" class="btn btn-primary col-md-12"></td>
	 				</tr>
	 			</table>
	 		</div>
		</form>
    </div>

 