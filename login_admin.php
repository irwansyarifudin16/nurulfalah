<?php
if(isset($_SESSION['login_user_admin']))
{
header("location: home_admin.php");
}
?>


<!DOCTYPE html>
<html>
<head>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <title>MI Nurul Falah Muncul</title>
</head>
<style>
  form {

  }
img.avatar {
    width: 100px;
    border-radius: 50%;
}

@media screen and (max-width: 100px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 50px;
    }
}
</style>
<body>


<div class="container-fluid" style="background-color: black">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a style="color: #F05F40" class="navbar-brand page-scroll" href="login_admin.php#page-top">Login Admin</a>
            </div>

        </div>
        <div class="container">
<h2 align="center"><font color="black">Login Admin</font><hr></h2>

 </div>
 
<form action="proses_login2_admin.php" method="POST">
<br>
<br>
  <div class="imgcontainer row text-center">
    <img src="img_avatar2.png" alt="Avatar" class="avatar">
  </div>
<br>
<br>
  <div class="row container row col-md-offset-5 col-md-2" align="center">
    <div class="form-group">
        <div class="row">
            <input type="text" class="form-control" placeholder="Enter Username" name="username_admin" required>    
        </div>
<br>
        <div class="row">
            <input type="password" class="form-control" placeholder="Enter Password" name="password_admin" required>    
        </div>
<br>
        <div class="row">
            <button class="btn btn-primary col-md-12" type="submit" name="login">Masuk</button>    
        </div>
<br>
            
    </div>

  
</body>
</html>