<?php
include "session_admin.php";
?>

<?php

$username=$_COOKIE['username_admin'];
 
?>

<!DOCTYPE html>
<html>
<head>
<title>Administrator</title>

<script language="javascript"> function usulan() { if (confirm ("Apakah Anda yakin akan menghapus usulan sekolah ini ?")) {
return true;
} 
else {
return false;
}
}
</script>

<script language="javascript"> function akun() { if (confirm ("Apakah Anda yakin akan menghapus akun ini ?")) {
return true;
} 
else {
return false;
}
}
</script>


<script language="javascript"> function tanya() { if (confirm ("Apakah Anda yakin akan menghapus kegiatan ini ?")) {
return true;
} 
else {
return false;
}
}
</script>

<script language="javascript"> function pesan() { if (confirm ("Apakah Anda yakin akan menghapus Pesan ini ?")) {
return true;
} 
else {
return false;
}
}
</script>

<script language="javascript"> function konfirm() { if (confirm ("Konfirmasi status donasi user ?")) {
return true;
} 
else {
return false;
}
}
</script>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
body {margin: 0;}

ul.sidenav {
    list-style-type: none;
    margin: 0;
    padding: 0;
    width: 25%;
    background-color:#d84315;
    position: fixed;
    height: 100%;
    overflow: auto;
}

ul.sidenav li a {
    display: block;
    color:  #fafafa;
    padding: 8px 16px;
    text-decoration: none;
}
 
ul.sidenav li a.active {
    background-color: #424242;
    color: white;
}

ul.sidenav li a:hover:not(.active) {
    background-color: #cfd8dc;
    color: #212121;
}

div.content {
    margin-left: 25%;
    padding: 1px 16px;
    height: 1000px;
}

@media screen and (max-width: 1050px){
    ul.sidenav {
        width:100%;
        height:auto;
        position:relative;
    }
    ul.sidenav li a {
        float: left;
        padding: 15px;
    }
    div.content {margin-left:0;}
}

@media screen and (max-width: 400px){
    ul.sidenav li a {
        text-align: center;
        float: none;
    }
}
</style>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>

img {
    border: 1px solid #424242;
    border-radius: 6px;
    padding: 5px;
}

 .nav-side-menu {
  overflow: auto;
  font-family: verdana;
  font-size: 12px;
  font-weight: 200;
  background-color: #2e353d;
  position: fixed;
  top: 0px;
  width: 300px;
  height: 100%;
  color: #e1ffff;
}
.nav-side-menu .brand {
  background-color: #F05F40;
  line-height: 50px;
  display: block;
  text-align: center;
  font-size: 14px;
}
.nav-side-menu .toggle-btn {
  display: none;
}
.nav-side-menu ul,
.nav-side-menu li {
  list-style: none;
  padding: 0px;
  margin: 0px;
  line-height: 35px;
  cursor: pointer;
  /*    
    .collapsed{
       .arrow:before{
                 font-family: FontAwesome;
                 content: "\f053";
                 display: inline-block;
                 padding-left:10px;
                 padding-right: 10px;
                 vertical-align: middle;
                 float:right;
            }
     }
*/
}
.nav-side-menu ul :not(collapsed) .arrow:before,
.nav-side-menu li :not(collapsed) .arrow:before {
  font-family: FontAwesome;
  content: "\f078";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
  float: right;
}
.nav-side-menu ul .active,
.nav-side-menu li .active {
  border-left: 3px solid #d19b3d;
  background-color: #4f5b69;
}
.nav-side-menu ul .sub-menu li.active,
.nav-side-menu li .sub-menu li.active {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li.active a,
.nav-side-menu li .sub-menu li.active a {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li,
.nav-side-menu li .sub-menu li {
  background-color: #181c20;
  border: none;
  line-height: 28px;
  border-bottom: 1px solid #23282e;
  margin-left: 0px;
}
.nav-side-menu ul .sub-menu li:hover,
.nav-side-menu li .sub-menu li:hover {
  background-color: #020203;
}

div.content {
    margin-left: 25%;
    padding: 1px 16px;
    height: 1000px;
}



.nav-side-menu ul .sub-menu li:before,
.nav-side-menu li .sub-menu li:before {
  font-family: FontAwesome;
  content: "\f105";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
}
.nav-side-menu li {
  padding-left: 0px;
  border-left: 3px solid #2e353d;
  border-bottom: 1px solid #23282e;
}
.nav-side-menu a {
  text-decoration: none;
  color: #e1ffff;
}
.nav-side-menu a i {
  padding-left: 10px;
  width: 20px;
  padding-right: 20px;
}

.nav-side-menu li a {
  text-decoration: none;
  color: #e1ffff;
}
.nav-side-menu li a i {
  padding-left: 10px;
  width: 20px;
  padding-right: 20px;
}
.nav-side-menu li:hover {
  border-left: 3px solid #d19b3d;
  background-color: #4f5b69;
  -webkit-transition: all 1s ease;
  -moz-transition: all 1s ease;
  -o-transition: all 1s ease;
  -ms-transition: all 1s ease;
  transition: all 1s ease;
}
@media (max-width: 767px) {
  .nav-side-menu {
    position: relative;
    width: 100%;
    margin-bottom: 10px;
  }
  .nav-side-menu .toggle-btn {
    display: block;
    cursor: pointer;
    position: absolute;
    right: 10px;
    top: 10px;
    z-index: 10 !important;
    padding: 3px;
    background-color: #ffffff;
    color: #000;
    width: 40px;
    text-align: center;
  }
  .brand {
    text-align: left !important;
    font-size: 22px;
    padding-left: 20px;
    line-height: 50px !important;
  }
}
@media (min-width: 767px) {
  .nav-side-menu .menu-list .menu-content {
    display: block;
  }
}
body {
  font-family: verdana;
  font-size: 12px;
  margin: 0px;
  padding: 0px;
}

table {
    border :0;
    width: 100%;
}

th, td {
      border :0;
    text-align:center;
    padding: 8px;
}

tr:nth-child(even){background-color: }

th {
    border :1;
    background-color: #cfd8dc;
    color: #212121;
    text-align: center
}

</style>

<style>
.button {
  display: inline-block;
  padding: 7px 18px;
  font-size: 12px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #424242;
  border: none;
  border-radius: 15px;
  box-shadow: 0 4px #999;
}

.button1 {
  display: inline-block;
  padding: 7px 18px;
  font-size: 12px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #37474f;
  border: none;
  border-radius: 15px;
  box-shadow: 0 4px #999;
}

.button:hover {background-color: #d84315}

.button:active {
  box-shadow: 0 5px #666;
  transform: translateY(2px);
}
</style>

<style>
#rcorners1 {
    cursor: pointer;
    border-radius: 25px;
    border: 2px solid #424242;
    padding: 20px; 
    width: 800px;
    height: 120px;   
    box-shadow: 0 5px #ff7043;
}

#rcorners1:hover {background-color: #424242;color: #fff;
}

#rcorners1:active {
  color: #fff;
  background-color: #424242;
  box-shadow: 0 5px #666;

  transform: translateY(4px);
}

#rcorners2 {
   border-radius: 25px;
    box-shadow: 0 5px #ff7043;
    border: 2px solid #424242;
    padding: 20px; 
    width: 800px;
    height: 150px       
}

#rcorners2:hover {background-color: #424242;color: #fff;}

#rcorners2:active {
  color: #fff;
  background-color: #424242;
  box-shadow: 0 5px #666;

  transform: translateY(4px);
}

p {
    text-indent: 50px;
    font-family: arial;
    font-size: 14px;
    text-align: justify;
    letter-spacing: 1px;
}
</style>

<style>
#notification_count
{
padding: 0px 3px 3px 7px;
background: #cc0000;
color: #ffffff;
font-weight: bold;
margin-left: 77px;
border-radius: 9px;
-moz-border-radius: 9px;
-webkit-border-radius: 9px;
position: absolute;
margin-top: -1px;
font-size: 10px;
}
</style>
 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" charset="utf-8">
 
function addmsg(type, msg){
 
$('#notification_count').html(msg);
 
}
 
function waitForMsg(){
 
$.ajax({
type: "GET",
url: "notif_pesan.php",
 
async: true,
cache: false,
timeout:50000,
 
success: function(data){
addmsg("new", data);
setTimeout(
waitForMsg,
1000
);
},
error: function(XMLHttpRequest, textStatus, errorThrown){
addmsg("error", textStatus + " (" + errorThrown + ")");
setTimeout(
waitForMsg,
15000);
}
});
};
 
$(document).ready(function(){
 
waitForMsg();
 
});
 
</script>

</head>
<body>

<ul class="sidenav">
  <li><a class="active" href="#home">Home</a></li>
  <li><span id="notification_count"></span>
  <a href="#pesan" id="notificationLink" onclick = "return getNotification()">Pesan</a></li>
  <li><a href="#data_donatur">Data Donatur <i><b>- member</b></i></a></li>
  <li><a href="#data_donatur_non_member">Data Donatur <i><b>- non member</b></i></a></li>
  <li><a href="#data_sumbangan_member">Data Sumbangan Barang <i><b>- member</b></i></a></li>
  <li><a href="#data_sumbangan_non_member">Data Sumbangan Barang <i><b>- non member</b></i></a></li>
  <li><a href="#data_kakak_asuh">Data Kakak Asuh</a></li>
  <li><a href="#data_pengasuhan">Data Pengasuhan</a></li>
  <li><a href="#data_kakak_asuh">Data Volunteer</a></li>
  <li><a href="#data_transaksi">Data Nominal Donasi</a></li>
  <li><a href="#data_kegiatan">Data Kegiatan</a></li>
  <li><a href="#data_mapel">Data Mata Pelajaran</a></li>
  <li><a href="#data_siswa">Data Siswa</a></li>
  <li><a href="#data_guru">Data Guru</a></li>
  <li><a href="logout_admin.php">Keluar</a></li>
</ul>

<div class="content" id="pesan">
  <h2>Pesan</h2>
<div id="HTMLnoti" style="textalign:center"></div>


<table  border='1' Width='800'>  
<tr>
    <th>ID pesan</th>
    <th>Pengirim</th>
    <th>Email</th>
    <th>Subjek</th>
    <th>Tanggal</th>
    <th colspan="2">Aksi</th>
    <th>Status</th>
    </tr>

<?php  

$queri_pesan="Select * from pesan" ; 

$hasil_pesan=mysqli_query($mysql, $queri_pesan);   

while ($data = mysqli_fetch_array ($hasil_pesan))
{
$id_pesan = $data['id_pesan'];
echo "    
        <tr>
        <td>".$data['id_pesan']."</td>
        <td>".$data['nama_pengirim']."</td>
        <td>".$data['email']."</td>
        <td>".$data['subjek']."</td>
        <td>".$data['tgl_pesan']."</td>
        <td><a href='lihat_kontak.php?id_pesan=$id_pesan'>Lihat pesan</a></td>
        <td><a href='delete_pesan.php?id_pesan=$id_pesan' onClick='return pesan()'>Delete</a></td>
        <td>".$data['status']."</td>
        </tr> 
        ";     
}
mysqli_free_result($hasil_pesan);
?>

</table>
</div>

<div class="content" id="home">
  <h2 style="margin: 50px 20px 20px 20px;">Halo <b><?php echo $login_session; ?></b>, Selamat Datang :)</h2>
  <p>Kamu sebagai Admin web ini. </p>
  <br>
<div style="margin-left: 100px;">
<?php
  if($login_session=='Abirachman')
  {
    echo "<a href ='daftar_admin.php'><button class='btn btn-primary col-md-offset-5 col-md-2' type='submit' name='login'>Daftar Admin Baru</button></a>";
  }
  else
  {

  }
  ?>
</div>
</div>
<div class="content" id="data_donatur">
  <h2>Data donatur</h2>
 
<table  border='1' Width='800'>  
<tr>
    <th>id_user</th>
    <th>nama</th>
    <th>username</th>
    <th>email</th>
    <th>alamat</th>
    <th>no_tlp</th>
    <th>pekerjaan</th>
    <th>foto</th> 
    <th colspan="2"> Aksi </th>
    </tr>

<?php  

$queri="Select * from user" ; 

$hasil=mysqli_query($mysql, $queri);   

while ($data = mysqli_fetch_array ($hasil)){
$id_user = $data['id_user'];
$foto = stripslashes ($data['foto']);
 echo "    
        <tr>
        <td>".$data['id_user']."</td>
        <td>".$data['nama']."</td>
        <td>".$data['username']."</td>
        <td>".$data['email']."</td>
        <td>".$data['alamat']."</td>
        <td>".$data['no_tlp']."</td>
        <td>".$data['pekerjaan']."</td>
        <td><img src='file/$foto' height='80px'></td>
        <td><a href='delete_donatur.php?id=$id_user' onClick='return akun()'>Delete</a></td>
        </tr> 
        ";     
}
?>
</table>

</div>

<div class="content" id="data_donatur_non_member">
  <h2>Data donatur ( non member )</h2>
 
<table  border='1' Width='800'>  
<tr>
    <th>Nama</th>
    <th>Email</th>
    <th>No Tlp</th>
    <th colspan="2"> Aksi </th>
    </tr>

<?php  

$queri="Select * from non_user" ; 

$hasil=mysqli_query($mysql, $queri);   

while ($data = mysqli_fetch_array ($hasil)){
$id_transaksi_non_user = $data['id_transaksi_non_user'];
 echo "    
        <tr>
        <td>".$data['nama_non_user']."</td>
        <td>".$data['email_non_user']."</td>
        <td>".$data['no_tlp_non_user']."</td>
        <td><a href='delete_donatur_nonuser.php?id=id_transaksi_non_user' onClick='return tanya()'>Delete</a></td>
        </tr> 
        ";     
}

mysqli_free_result($hasil);

?>
</table>

</div>

<div class="content" id="data_kakak_asuh">
  <h2>Data Kakak Asuh</h2>

<a href="pengasuhan.php"><input type="submit" value="Tambah Pengasuhan" class="button"></a><br><br>
 
<table  border='1' Width='800'>  
<tr>
    <th>id_user</th>
    <th>nama</th>
    <th>username</th>
    <th>email</th>
    <th>no_telp</th>
    <th>pekerjaan</th>
    <th>foto</th> 
    <th colspan="2"> Aksi </th>
    </tr>

<?php  

$queri="Select * from kakak_asuh" ; 

$hasil=mysqli_query($mysql, $queri);   

while ($data = mysqli_fetch_array ($hasil)){
$id_kakak_asuh = $data['id_kakak_asuh'];
$foto = stripslashes ($data['foto']);
 echo "    
        <tr>
        <td>".$data['id_kakak_asuh']."</td>
        <td>".$data['nama']."</td>
        <td>".$data['username']."</td>
        <td>".$data['email']."</td>
        <td>".$data['no_telp']."</td>
        <td>".$data['pekerjaan']."</td>
        <td><img src='file/$foto' height='80px'></td>
        <td><a href='delete_donatur.php?id=$id_kakak_asuh' onClick='return akun()'>Delete</a></td>
        </tr> 
        ";     
}

mysqli_free_result($hasil);

?>
</table>

</div>

<div class="content" id="data_sumbangan_member">
  <h2>Data Sumbangan</h2>

<?php

//$sql = "SELECT id_barang,jenis_barang,nama_barang,jumlah_barang,tgl_masuk,foto,status from daftar_barang_sumbangan";

$sql_sumem = "SELECT id_barang,jenis_barang,nama_barang,jumlah_barang,tgl_masuk,foto_barang,status, nama from daftar_barang_sumbangan INNER JOIN user ON daftar_barang_sumbangan.id_user=user.id_user";
$queri_sumem = mysqli_query($mysql, $sql_sumem);

$sql = "SELECT id_barang,jenis_barang,nama_barang,jumlah_barang,tgl_masuk,foto_barang,status, nama from daftar_barang_sumbangan INNER JOIN user ON daftar_barang_sumbangan.id_user=user.id_user LIMIT 0,8";
//$sql = "SELECT id_transaksi,jml_transaksi,nama_bank,no_rek,bukti_transaksi,status_transaksi,tgl_transaksi,nama from transaksi INNER JOIN user ON transaksi.id_user=user.id_user";

$queri = mysqli_query($mysql, $sql);

  if ($queri)

{

  $hitung_transaksi=mysqli_num_rows($queri);
  

  if($hitung_transaksi > 0)
  {
      echo "<table border='0'>
        <thead>
          <tr>
          <th>Nama </th>
          <th>ID Barang</th>
          <th>Jenis Barang</th>
          <th>Nama Barang</th>
          <th>Tanggal Sumbangan</th>
          <th>Foto</th>
          <th>Status Sumbangan</th>
          </tr>
        </thead>
        <tbody>";

    
      while ($row = mysqli_fetch_assoc($queri))
      {
        $id_barang = $row['id_barang'];
        $foto = stripslashes ($row['foto_barang']);
         echo "<tr>
        <td>".$row['nama']."</td>
        <td>".$row['id_barang']."</td>
        <td>".$row['jenis_barang']."</td>
        <td>".$row['nama_barang']."</td>
        <td>".$row['jumlah_barang']."</td>
        <td>".$row['tgl_masuk']."</td>
        <td><img src='file/$foto' height='40px' width='40px'></td>
        <td>".$row['status']."</td>
        </tr> 
      </tr>";
      }
        echo "
        </tbody>
        </table>.<br>";

$count1 = mysqli_num_rows($queri);
$count2 = mysqli_num_rows($queri_sumem);
$selisih = $count2 - $count1;        
echo "<ul><li>Total sumbangan barang dari Member sebanyak <b>$count2 orang</b></li></ul>";

if($count2 > 8)
{
echo "<ul><li>Masih ada <b>$selisih orang</b> yang belum dilihat. </li></ul><a href=''>Lihat selengkapnya</a>";
}
else
{

}

mysqli_free_result($queri);
}

else
  {
    echo "Belum ada Sumbangan"."<br>"."<br>";
  }

}

  else
{
    echo "gagal masuk";
}
  
  ?>
</div>

<div class="content" id="data_sumbangan_non_member">
  <h2>Data Sumbangan - Non member</h2>

<?php

$sql_total = "SELECT id_barang_nonuser,nama_nonuser, no_telp_barang_nonuser, jenis_barang_nonuser, nama_barang_nonuser, jumlah_barang_nonuser, tgl_masuk_nonuser, foto_nonuser, status_nonuser from daftar_sumbangan_nonuser";
$queri_total = mysqli_query($mysql, $sql_total);

$sql = "SELECT id_barang_nonuser,nama_nonuser, no_telp_barang_nonuser, jenis_barang_nonuser, nama_barang_nonuser, jumlah_barang_nonuser, tgl_masuk_nonuser, foto_nonuser, status_nonuser from daftar_sumbangan_nonuser LIMIT 0,8";

$queri = mysqli_query($mysql, $sql);

  if ($queri)

{

  $hitung_transaksi=mysqli_num_rows($queri);
  

  if($hitung_transaksi > 0)
  {
      echo "<table border='0'>
        <thead>
          <tr>
          <th>Nama </th>
          <th>ID Barang</th>
          <th>Jenis Barang</th>
          <th>Nama Barang</th>
          <th>Tanggal Sumbangan</th>
          <th>Foto</th>
          <th>Status Sumbangan</th>
          </tr>
        </thead>
        <tbody>";

    
      while ($row = mysqli_fetch_assoc($queri))
      {
        $id_barang = $row['id_barang_nonuser'];
        $foto = stripslashes ($row['foto_nonuser']);
         echo "<tr>
        <td>".$row['nama_nonuser']."</td>
        <td>".$row['id_barang_nonuser']."</td>
        <td>".$row['jenis_barang_nonuser']."</td>
        <td>".$row['nama_barang_nonuser']."</td>
        <td>".$row['jumlah_barang_nonuser']."</td>
        <td>".$row['tgl_masuk_nonuser']."</td>
        <td><img src='file/$foto' height='40px' width='40px'></td>
        <td>".$row['status_nonuser']."</td>
        </tr> 
      </tr>";
      }
        echo "
        </tbody>
        </table>.<br>";
$count1 = mysqli_num_rows($queri);
$count2 = mysqli_num_rows($queri_total);
$selisih = $count2 - $count1;        
echo "<ul><li>Total sumbangan barang dari Non Member sebanyak <b>$count2 orang</b></li></ul>";
echo "<ul><li>Masih ada <b>$selisih orang</b> yang belum dilihat. </li></ul><a href=''>Lihat selengkapnya</a>";
      mysqli_free_result($queri);
}


else
  {
    echo "Belum ada Sumbangan"."<br>"."<br>";
  }

}

  else
{
    echo "gagal masuk";
}
  
  ?>
</div>

<div class="content" id="data_transaksi">
  <h2>Data Transaksi</h2>

<?php

$sql = "SELECT id_transaksi,jml_transaksi,nama_bank,no_rek,bukti_transaksi,status_transaksi,tgl_transaksi,nama from transaksi INNER JOIN user ON transaksi.id_user=user.id_user";
    
$queri = mysqli_query($mysql, $sql);

  if ($queri)

{

  $hitung_transaksi=mysqli_num_rows($queri);
  

  if($hitung_transaksi > 0)
  {

   
      echo "<table border='0'>
        <thead>
          <tr>
          <th>Nama Donatur</th>
          <th>ID Transaksi</th>
          <th>Nominal Transaksi</th>
          <th>Nama Bank</th>
          <th>Nomor Rekening</th>
          <th>Bukti Transaksi</th>
          <th>Status Transaksi</th>
          <th> Aksi </th>
          </tr>
        </thead>
        <tbody>";

    
      while ($row = mysqli_fetch_assoc($queri))
      {
        $id_transaksi = $row['id_transaksi'];
        $bukti_transaksi = stripslashes ($row['bukti_transaksi']);
         echo "<tr>
        <td>".$row['nama']."</td>
        <td>".$row['id_transaksi']."</td>
        <td>".$row['jml_transaksi']."</td>
        <td>".$row['nama_bank']."</td>
        <td>".$row['no_rek']."</td>
        <td><img src='file/$bukti_transaksi' height='40px' width='40px'></td>
        <td>".$row['status_transaksi']."</td>
        <td><a href='konfirmasi_berhasil.php?id_konfirm=$id_transaksi' onClick='return konfirm()'>konfirm</a></td>
        </tr> 
      </tr>";
      }
        echo "
        </tbody>
        </table>.<br>";

/*$count = mysqli_num_rows($queri);
        
echo "<ul><li>Kamu sudah berdonasi sebanyak : <b>$count kali</b></li></ul>";
        
$r=mysqli_query($mysql, "select id_transaksi, SUM(jml_transaksi) from transaksi where id_user='$id_user'");

        while($t=mysqli_fetch_array($r))

        {  
            $x=$t['SUM(jml_transaksi)'];

            echo "<ul>
                  <li>Nominal uang yang didonasikan saat ini : <b>Rp $x</b></li>
                  </ul>
                  ";
        }

            echo "Kamu juga bisa <b><a href='ambil_data.php'>Cetak bukti Donasi </a></b> <br><br>";*/

mysqli_free_result($queri);
}

else
  {
    echo "Belum ada transaksi"."<br>"."<br>";
  }

}

  else
{
    echo "gagal masuk";
}
  
/*echo "<b>Donatur ( Member )</b><br><hr>";

      $sql = "SELECT id_transaksi,jml_transaksi,nama_bank,no_rek,bukti_transaksi,status_transaksi,tgl_transaksi,nama from transaksi INNER JOIN user ON transaksi.id_user=user.id_user";

      $query = mysqli_query($mysql, $sql);

    if(mysqli_num_rows($query)==0)
    
    {
        echo "Belum ada donasi atau sudah terhapus"; 

    }
       
    else
    {

    echo "<table border='0'>
        <thead>
          <tr>
          <th>Nama Donatur</th>
          <th>ID Transaksi</th>
          <th>Nominal Transaksi</th>
          <th>Nama Bank</th>
          <th>Nomor Rekening</th>
          <th>Bukti Transaksi</th>
          <th>Status Transaksi</th>
          <th> Aksi </th>
          </tr>
        </thead>
        <tbody>";
    
      while ($row = mysqli_fetch_assoc($query))
      {
        $id_transaksi = $row['id_transaksi'];
        $bukti_transaksi = stripslashes ($row['bukti_transaksi']);
         echo "<tr>
        <td>".$row['nama']."</td>
        <td>".$row['id_transaksi']."</td>
        <td>".$row['jml_transaksi']."</td>
        <td>".$row['nama_bank']."</td>
        <td>".$row['no_rek']."</td>
        <td><img src='file/$bukti_transaksi' height='40px' width='40px'></td>
        <td>".$row['status_transaksi']."</td>
        <td><a href='konfirmasi_berhasil.php?id_konfirm=$id_transaksi' onClick='return konfirm()'>konfirm</a></td>
        </tr> 
      </tr>";
      }
        echo "
        </tbody>
        </table>.<br>";

 
        $r=mysqli_query("select SUM(jml_transaksi) from transaksi");

        while($t=mysqli_fetch_array($r))
        {  
            $x=$t['SUM(jml_transaksi)'];
            echo "<ul>
                  <li>Total uang donasi dari Donatur ( member ) saat ini : <b>Rp $x</b></li>
                  </ul><br>
                  ";
        }

        mysqli_free_result($query);

        mysqli_close($mysql);
     }
?>

<?php
  echo "<br><br><b>Donatur ( non member)</b><br><hr>";

      $sql_non = "SELECT id_transaksi_non_user, nama_non_user, tgl_transaksi_non_user, jml_transaksi_non_user, bank_non_user, no_rek_non_user FROM non_user";
    
      $query_non = mysqli_query($mysql, $sql_non);

    if(mysqli_num_rows($query_non)==0)
      {
        echo "Belum ada donasi dari donatur member"; }  
    else
    {

    echo "<table border='0'>
        <thead>
          <tr>
          <th>ID Transaksi ( non member )</th>
          <th>Nama</th>
          <th>Tgl Transaksi</th>
          <th>Nominal Transaksi</th>
          <th>Nama Bank</th>
          <th>No Rekening</th>
          </tr>
        </thead>
        <tbody>";
    
      while ($row = mysqli_fetch_assoc($query_non))
      {
        $id_transaksi_non_user = $row['id_transaksi_non_user'];
         echo "<tr>
        <td>".$row['id_transaksi_non_user']."</td>
        <td>".$row['nama_non_user']."</td>
        <td>".$row['tgl_transaksi_non_user']."</td>
        <td>".$row['jml_transaksi_non_user']."</td>
        <td>".$row['bank_non_user']."</td>
        <td>".$row['no_rek_non_user']."</td>
        </tr> 
      </tr>";
      }
        echo "
        </tbody>
        </table>.<br>";

 
        $a=mysqli_query("select SUM(jml_transaksi_non_user) from non_user");

        while($i=mysqli_fetch_array($a))
        {  
            $y=$i['SUM(jml_transaksi_non_user)'];
            echo "<ul>
                  <li>Total uang donasi dari Donatur ( non member ) saat ini : <b>Rp $y</b></li>
                  </ul>
                  ";
        }

        mysqli_free_result($query_non);

        mysqli_close($mysql);
     }*/
  ?>

</div>

<div class="content" id="data_kegiatan">
  <h2>Kegiatan</h2><br>

  <form action="tambah_kegiatan.php" method="post">
  <button type="submit" name="tambah_kegiatan" class="button">Tambah Kegiatan</button>
  </form><br>

<?php

  $queri="Select * from Kegiatan" ; 

  $hasil=mysqli_query($mysql, $queri);   

  if(mysqli_num_rows($hasil)==0)
  {
    echo "Belum ada kegiatan"; 
  }
   
  else 
  { 

    echo "<table  border='1' Width='800'>  
    <tr>
    <th>id_kegiatan</th>
    <th>nama_kegiatan</th>
    <th>tgl_kegiatan</th>
    <th>keterangan</th>
    <th>kategori</th>
    <th>Foto</th>
    <th colspan='2'> Aksi </th>
    </tr>";


  while ($data = mysqli_fetch_array ($hasil)){
  $id_kegiatan = $data['id_kegiatan'];
  $foto = stripslashes ($data['foto_kegiatan']);
  echo "    
        <tr>
        <td>".$data['id_kegiatan']."</td>
        <td>".$data['nama_kegiatan']."</td>
        <td>".$data['tgl_kegiatan']."</td>
        <td>".$data['keterangan']."</td>
        <td>".$data['kategori']."</td>
        <td><img src='file/$foto' height='80px'></td>        
        <td><a href='update_kegiatan.php?id_kegiatan=$id_kegiatan'>update</td>
        <td><a href='delete_kegiatan.php?id_kegiatan=$id_kegiatan' onClick='return tanya()'>Delete</a></td>
        </tr> 
        ";     
  }

  mysqli_free_result($hasil);

  }
  ?>
  <br>
  </table>
  <br><br>
</div>

<div class="content" id="data_siswa">
  <h2>Lihat Siswa</h2><br>

  <form action="tambah_siswa.php" method="post">
  <button type="submit" name="tambah_kegiatan" class="button">Tambah Siswa</button>
  </form><br>

<?php

$sql = "SELECT NIS, nama_siswa, kelas, nama_orangtua, no_telp, tgl_lahir, angkatan, alamat, foto from siswa";
    
$queri = mysqli_query($mysql, $sql);

  if ($queri)

{

  $hitung_transaksi=mysqli_num_rows($queri);
  

  if($hitung_transaksi > 0)
  {
   
   echo "<a href='lihat_kelas.php?kelas=1' target = '_blank'>Lihat siswa kelas 1</a><br>";
   echo "<a href='lihat_kelas.php?kelas=2' target = '_blank'>Lihat siswa kelas 2</a><br>";
   echo "<a href='lihat_kelas.php?kelas=3' target = '_blank'>Lihat siswa kelas 3</a><br>";
   echo "<a href='lihat_kelas.php?kelas=4' target = '_blank'>Lihat siswa kelas 4</a><br>";
   echo "<a href='lihat_kelas.php?kelas=5' target = '_blank'>Lihat siswa kelas 5</a><br>";
   echo "<a href='lihat_kelas.php?kelas=6' target = '_blank'>Lihat siswa kelas 6</a><br>";
   /*      
        <thead>
          <tr>
          <th>NISN</th>
          <th>Nama</th>
          <th>kelas</th>
          <th>Orang Tua</th>
          <th>Kontak</th>
          <th>TTL</th>
          <th>Angkatan</th>
          <th>Alamat</th>
          <th>Foto</th>

          </tr>
        </thead>
        <tbody>';
    
      while ($row = mysqli_fetch_assoc($queri))
      {
        $foto = stripslashes ($row['foto']);
         echo "<tr>
        <td>".$row['NIS']."</td>
        <td>".$row['nama_siswa']."</td>
        <td>".$row['kelas']."</td>
        <td>".$row['nama_orangtua']."</td>
        <td>".$row['no_telp']."</td>
        <td>".$row['tgl_lahir']."</td>
        <td>".$row['angkatan']."</td>
        <td>".$row['alamat']."</td>
        <td><img src='file/$foto' height='40px' width='40px'></td>
        </tr>";
      }
        echo '
        </tbody>
        </table>.<br><hr><br>';
 
mysqli_free_result($queri);*/

}

  else
  {
    echo "Belum ada siswa hehe"."<br>"."<br>";
    echo '<a href="tambah_siswa.php"><input name="siswa" type="submit" id="siswa" value="Tambah Siswa" class="button"></a>';

  }

}

  else
{
    echo "gagal masuk";
}

?>
  <br>
  </table>
  <br><br>
</div>

<div class="content" id="data_guru">
  <h2>Data Guru</h2>


<?php
$sql_guru = "SELECT NUPTK,nama_guru,tgl_lahir,jenis_kelamin,no_telp, lama_mengajar, status, foto, nama_mapel from guru INNER JOIN mata_pelajaran ON guru.id_mapel=mata_pelajaran.id_mapel";
$queri_guru = mysqli_query($mysql, $sql_guru);

$sql = "SELECT NUPTK,nama_guru,tgl_lahir,jenis_kelamin,no_telp, lama_mengajar, status, foto, nama_mapel from guru INNER JOIN mata_pelajaran ON guru.id_mapel=mata_pelajaran.id_mapel LIMIT 0,8";
$queri = mysqli_query($mysql, $sql);

  if ($queri)

{

  $hitung_guru=mysqli_num_rows($queri);
  

  if($hitung_guru > 0)
  {
    echo '<a href="tambah_guru.php"><input type="submit" value="Tambah Guru" class="button"></a><br><br>';

    echo "<table>  
    <tr>
    <th>NIP</th>
    <th>Nama</th>
    <th>Bidang Studi</th>
    <th>No Telepon</th>
    <th>Foto</th>
    </tr>";


  while ($data = mysqli_fetch_array ($queri)){
  echo "    
        <tr>
        <td>".$data['NUPTK']."</td>
        <td>".$data['nama_guru']."</td>
        <td>".$data['nama_mapel']."</td>
        <td>".$data['no_telp']."</td>
        <td><img src='file/$foto' height='40px' width='40px'></td>
        </tr> 
        ";     
  }
        echo "
        </tbody>
        </table>.<br>";

$count1 = mysqli_num_rows($queri);
$count2 = mysqli_num_rows($queri_guru);
$selisih = $count2 - $count1;        
echo "<ul><li>Jumlah Guru MI Nurul Falah Muncul sebanyak <b>$count2 orang</b></li></ul>";

if($count2 > 8)
{
echo "<ul><li>Masih ada <b>$selisih orang</b> yang belum dilihat. </li></ul><a href='lihat_guru.php'>Lihat selengkapnya</a>";
}
else
{

}

  //mysqli_free_result($hasil);

  }

  else
  {
    echo "Belum ada guru hehe"."<br>"."<br>";
    echo '<a href="tambah_guru.php" target ="_blank"><input name="guru" type="submit" id="guru" value="Tambah Guru" class="button"></a>';

  }

}

  else
{
    echo "gagal masuk";
}

?>
  <br>
  </table>
  <br><br>
</div>

<div class="content" id="data_mapel">
  <h2>Data Mata Pelajaran</h2>


<?php

$sql = "SELECT *from mata_pelajaran";
    
$queri = mysqli_query($mysql, $sql);

  if ($queri)

{

  $hitung_guru=mysqli_num_rows($queri);
  

  if($hitung_guru > 0)
  {
    echo '<a href="tambah_pelajaran.php"><input type="submit" value="Tambah Pelajaran" class="button"></a><br>';

    echo "<table>  
    <tr>
    <th>ID Mapel</th>
    <th>Nama Mapel</th>
    </tr>";


  while ($data = mysqli_fetch_array ($queri)){
  $id_mapel = $data['id_mapel'];
  $nama_mapel = stripslashes ($data['nama_mapel']);
  echo "    
        <tr>
        <td>".$data['id_mapel']."</td>
        <td>".$data['nama_mapel']."</td>
        </tr> 
        ";     
  }

  //mysqli_free_result($hasil);

  }

  else
  {
    echo "Belum ada guru hehe"."<br>"."<br>";
    echo '<a href="tambah_pelajaran.php"><input name="guru" type="submit" id="guru" value="Tambah Guru" class="button"></a>';

  }

}

  else
{
    echo "gagal masuk";
}

?>
  <br>
  </table>
  <br><br>
</div>
</body>
</html>


