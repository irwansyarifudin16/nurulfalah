<?php 
include "session_kakakasuh.php";  
       
?>

<html>
<head>
	<title>Indobisa - Donasi</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/creative.css" rel="stylesheet">
    <link rel="stylesheet"  href="css/animate.css">
    <link rel="stylesheet"  href="css/style.css">
</head>
<body bgcolor="#F05F40">

<script type="text/javascript">
            function kali()
            {
                var nilai1=document.getElementById("val1").value;
                var nilai2=document.getElementById("val2").value;
                var kali=parseInt(nilai1)*parseInt(nilai2);
                document.getElementById("hasil").value=parseInt(kali);
            }
            function refresh()
            {
                document.getElementById("val1").value="";
                document.getElementById("val2").value="";
                document.getElementById("hasil").value="";
            }
</script>

 	<div class="container">
        <div class="row">
            <div class="text-center">
           <!--  	<img src="img/logo.jpeg"> -->
           		<br>
            	<h2 class="section-heading">Ayo Donasi</h2>
               	<hr class="primary">
            </div>
        </div>


      	<form action="proses_donasi_pertama_ka.php" method="POST">
 		
 		<div class="from-group col-md-offset-4 col-md-4">
       	<br>
        <label class="label-username" name="username">
          Kamu donasi untuk berapa bulan ?
          <br>
        </label>
        <br>
        <select name="lama_donasi" id="val1" onkeyup="kali()">
        <?php
        for($i=1; $i<=12; $i++)
        {
        echo "<option value='".$i."'>".$i."</option>";
        }
        ?>
        </select>
     	</div>

	 	<div class="from-group col-md-offset-4 col-md-4"><br>
	 			<label class="label-username" name="username">
	 				Masukan donasi
	 				<i>Donasi sebesar Rp 15.000 / bulan</i>
	 			</label>
	 			<br>
	 			<input type="number" id="val2" onkeyup="kali()" class="form-control" placeholder="jumlah donasi ( minimal 15000 )" min=14999 required>
	 	</div>

		<div class="from-group col-md-offset-4 col-md-4"><br>
	 			<label class="label-username" name="username">
	 				Total Donasi
	 				<i>Donasi kamu</i>
	 			</label>
	 			<br>
	 			<input type="text" name="jml_transaksi_ka" id="hasil" class="form-control" placeholder="diisi ya ckckck">
	 	</div>

	 		<div class="from-group col-md-offset-4 col-md-4"><br>
	 			<label class="label-username" name="username">
	 				Nama Bank Rekening	
	 				<br>
	 			</label>
	 			<br>
	 			<select type="text" name="nama_bank_ka" class="form-control" placeholder="nama_bank" required>
				<option value="BRI SYARIAH">BRI SYARIAH</option>
				<option value="BRI">BRI</option>
				<option value="BNI SYARIAH">BNI SYARIAH</option>
				<option value="MANDIRI SYARIAH">MANDIRI SYARIAH</option>
				<option value="MANDIRI">MANDIRI</option>
				<option value="BCA">BCA</option>
				<option value="BANK DKI">BANK DKI</option>
				<option value="PERMATA BANK">PERMATA BANK</option>


	 			<br>
	 		</div>


	 		<br>
	 		<div class="from-group col-md-offset-4 col-md-4">
	 			<br>
	 			<input type="number" name="no_rek_ka" class="form-control" placeholder="isi nomor rekening" value="isi nomor rekening" required>
	 		</div>	 
	 		<div class="from-group col-md-offset-4 col-md-4">
	 		<br><br>
	 			<center><table border="0"></center>
	 			<tr><td><input type="submit" value="Lanjut" name="lanjut" class="btn btn-primary col-md-12"></td>
	 			<td>&nbsp</td>
	 				<td><input type="reset" value="Batal" name="Reset" class="btn btn-primary col-md-12"></td>
	 				</tr>
	 			</table>
	 		</div>
		</form>
    </div>

 